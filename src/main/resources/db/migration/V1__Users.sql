CREATE TABLE users
(
    user_id        UUID        PRIMARY KEY,
    version        UUID        NOT NULL,
    created        TIMESTAMPTZ NOT NULL,
    updated        TIMESTAMPTZ NOT NULL,
    name           TEXT        NOT NULL,
    email          TEXT        NOT NULL UNIQUE,
    authentication JSONB       NOT NULL
);
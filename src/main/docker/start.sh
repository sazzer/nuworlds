#!/bin/sh

if [ -f /opt/nuworlds/application.properties.template ]; then
    envsubst < /opt/nuworlds/application.properties.template > /opt/nuworlds/application.properties
    cat /opt/nuworlds/application.properties
fi

java -jar service.jar

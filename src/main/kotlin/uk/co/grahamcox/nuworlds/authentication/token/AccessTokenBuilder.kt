package uk.co.grahamcox.nuworlds.authentication.token

import uk.co.grahamcox.nuworlds.users.UserModel
import java.util.*

/**
 * Means to build access tokens for users
 */
class AccessTokenBuilder {
    /**
     * Build the access token for the user
     * @param user The user
     * @return the access token
     */
    fun buildAccessToken(user: UserModel) = AccessToken(
            tokenId = UUID.randomUUID(),
            userId = user.identity.id
    )
}
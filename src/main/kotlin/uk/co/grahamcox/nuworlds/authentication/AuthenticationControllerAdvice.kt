package uk.co.grahamcox.nuworlds.authentication

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.servlet.ModelAndView
import uk.co.grahamcox.nuworlds.authentication.external.ExternalAuthenticationProviderRegistry
import uk.co.grahamcox.nuworlds.authentication.token.InvalidAccessTokenException
import uk.co.grahamcox.nuworlds.authentication.token.MissingAccessTokenException
import javax.servlet.http.Cookie
import javax.servlet.http.HttpServletResponse

/**
 * Controller Advice to ensure that the authentication details are available to the views
 */
@ControllerAdvice
class AuthenticationControllerAdvice(private val registry: ExternalAuthenticationProviderRegistry) {
    /**
     * Get the list of authentication providers that we can work with
     */
    @ModelAttribute("authenticationProviders")
    fun authenticationProviders() = registry.providerIds

    /**
     * Exception handler to indicate that a request for a page failed because the user wasn't logged in
     */
    @ExceptionHandler(MissingAccessTokenException::class, InvalidAccessTokenException::class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    fun handleMissingAccessToken(response: HttpServletResponse): ModelAndView {
        val authCookie = Cookie("nuworlds_access_token", null)
        authCookie.isHttpOnly = true
        authCookie.path = "/"
        authCookie.maxAge = 0

        response.addCookie(authCookie)

        return ModelAndView("unauthenticatedError", mapOf("authenticationProviders" to registry.providerIds))
    }
}
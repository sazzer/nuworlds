package uk.co.grahamcox.nuworlds.authentication.external

import org.slf4j.LoggerFactory

/**
 * Registry of External Authentication Providers to work with
 */
class ExternalAuthenticationProviderRegistry(private val providers: List<ExternalAuthenticationProvider>) {
    companion object {
        /** The logger to use */
        private val LOG = LoggerFactory.getLogger(ExternalAuthenticationProviderRegistry::class.java)
    }

    init {
        LOG.debug("Constructing the registry with providers: {}", providers)
    }

    /** The list of Provider IDs that we can use */
    val providerIds = providers.map(ExternalAuthenticationProvider::providerId)

    /**
     * Get the External Authentication Provider that has the given ID
     * @param providerId The ID of the provider to get
     */
    operator fun get(providerId: String) : ExternalAuthenticationProvider? =
            providers.firstOrNull { it.providerId == providerId}
}
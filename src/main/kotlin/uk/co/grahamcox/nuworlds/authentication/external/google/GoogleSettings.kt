package uk.co.grahamcox.nuworlds.authentication.external.google

import java.net.URI

/**
 * Settings for authenticating via Google
 * @property clientId The Client ID to use
 * @property clientSecret The Client Secret to use
 * @property callbackUrl The Callback URL to use
 * @property redirectUrl The Redirect URL to use to start authentication
 * @property tokenUrl The Token URL to use to complete authentication
 */
data class GoogleSettings(
        val clientId: String,
        val clientSecret: String,
        val callbackUrl: URI,
        val redirectUrl: URI,
        val tokenUrl: URI
) {
    /** Indication of whether we are fully configured or not */
    val configured = clientId.isNotBlank() && clientSecret.isNotBlank()
}
package uk.co.grahamcox.nuworlds.authentication.external

import java.net.URI

/**
 * The details needed to start authenticating a user with an external authentication provider
 */
data class StartAuthenticationResult(
        val redirectUrl: URI
)
package uk.co.grahamcox.nuworlds.authentication.external.google

import org.springframework.web.util.UriComponentsBuilder
import uk.co.grahamcox.nuworlds.authentication.external.StartAuthenticationResult
import uk.co.grahamcox.nuworlds.authentication.external.StartAuthenticationStrategy
import java.net.URI
import java.util.*

/**
 * Implementation of the Start Authentication Strategy for working with Google
 * @property settings The settings for authenticating with Google
 */
class GoogleStartAuthenticationStrategy(private val settings: GoogleSettings) : StartAuthenticationStrategy {
    /**
     * Start authentication with the external authentication provider
     */
    override fun startAuthentication(): StartAuthenticationResult {
        val authenticationUri: URI = UriComponentsBuilder.fromUri(settings.redirectUrl)
                .queryParam("client_id", settings.clientId)
                .queryParam("redirect_uri", settings.callbackUrl)
                .queryParam("response_type", "code")
                .queryParam("scope", "openid email profile")
                .queryParam("state", UUID.randomUUID().toString())
                .build()
                .toUri()

        return StartAuthenticationResult(authenticationUri)
    }
}
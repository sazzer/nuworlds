package uk.co.grahamcox.nuworlds.authentication.external.google

import com.auth0.jwt.JWT
import com.auth0.jwt.exceptions.JWTDecodeException
import org.slf4j.LoggerFactory
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType
import org.springframework.http.RequestEntity
import org.springframework.util.LinkedMultiValueMap
import org.springframework.web.client.RestClientException
import org.springframework.web.client.RestOperations
import uk.co.grahamcox.nuworlds.authentication.external.CompleteAuthenticationStrategy
import uk.co.grahamcox.nuworlds.authentication.external.ExternalProviderFailureException
import uk.co.grahamcox.nuworlds.authentication.external.ExternalUser

/**
 * Implementation of the Complete Authentication Strategy for working with Google
 * @property settings The settings for authenticating with Google
 */
class GoogleCompleteAuthenticationStrategy(
        private val settings: GoogleSettings,
        private val restTemplate: RestOperations) : CompleteAuthenticationStrategy {
    companion object {
        /** The logger to use */
        private val LOG = LoggerFactory.getLogger(GoogleCompleteAuthenticationStrategy::class.java)
    }

    /**
     * Complete authentication with the external authentication provider
     * @param params The parameters received from the external authentication provider
     * @return The external user that has authenticated
     */
    override fun completeAuthentication(params: Map<String, String>): ExternalUser {
        val code = params["code"] ?: throw ExternalProviderFailureException("Missing required parameter: code")
        LOG.debug("Requesting access token for authentication code: {}", code)

        val params = mapOf(
                "code" to code,
                "client_id" to settings.clientId,
                "client_secret" to settings.clientSecret,
                "redirect_uri" to settings.callbackUrl.toString(),
                "grant_type" to "authorization_code"
        )
        val requestHeaders = HttpHeaders()
        requestHeaders.contentType = MediaType.APPLICATION_FORM_URLENCODED

        val tokenResponse = try {
            restTemplate.exchange(
                    RequestEntity(
                            LinkedMultiValueMap(params.mapValues { listOf(it.value) }),
                            requestHeaders,
                            HttpMethod.POST,
                            settings.tokenUrl),
                    GoogleAccessToken::class.java)
        } catch (e: RestClientException) {
            LOG.warn("Error calling Google to get access token", e)
            throw ExternalProviderFailureException("Error getting Access Token")
        }

        LOG.debug("Received token response: {}", tokenResponse)

        val accessToken = tokenResponse.body ?: throw ExternalProviderFailureException("No payload returned from Google")
        accessToken.idToken ?: throw ExternalProviderFailureException("No ID Token returned from Google")

        val decodedIdToken = try {
            JWT.decode(accessToken.idToken)
        } catch (e: JWTDecodeException) {
            LOG.warn("Received malformed ID Token: {}", accessToken.idToken, e)
            throw ExternalProviderFailureException("Received malformed ID Token")
        }

        LOG.debug("Parsed ID Token: {}", decodedIdToken.claims.mapValues { it.value.asString() })

        return ExternalUser(
                userId = decodedIdToken.subject ?: throw ExternalProviderFailureException("No Subject present in ID Token"),
                name = decodedIdToken.getClaim("name").asString() ?: throw ExternalProviderFailureException("No Name present in ID Token"),
                email = decodedIdToken.getClaim("email").asString() ?: throw ExternalProviderFailureException("No Email Address present in ID Token"),
                displayName = decodedIdToken.getClaim("email").asString()
        )
    }
}
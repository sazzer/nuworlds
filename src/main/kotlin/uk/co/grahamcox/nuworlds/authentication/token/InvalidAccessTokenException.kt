package uk.co.grahamcox.nuworlds.authentication.token

/**
 * Exception to indicate that an access token was invalid
 */
class InvalidAccessTokenException(message: String) : Exception(message)
package uk.co.grahamcox.nuworlds.authentication

import org.slf4j.LoggerFactory
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.view.RedirectView
import uk.co.grahamcox.nuworlds.authentication.external.ExternalAuthenticationProviderRegistry
import uk.co.grahamcox.nuworlds.authentication.external.ExternalProviderFailureException
import uk.co.grahamcox.nuworlds.authentication.token.AccessTokenBuilder
import uk.co.grahamcox.nuworlds.authentication.token.AccessTokenSerializer
import uk.co.grahamcox.nuworlds.users.AuthenticationDetails
import uk.co.grahamcox.nuworlds.users.UserData
import uk.co.grahamcox.nuworlds.users.UserService
import javax.servlet.http.Cookie
import javax.servlet.http.HttpServletResponse

/**
 * Controller for authenticating the user
 */
@Controller
class AuthenticationController(
        private val registry: ExternalAuthenticationProviderRegistry,
        private val userService: UserService,
        private val accessTokenBuilder: AccessTokenBuilder,
        private val accessTokenSerializer: AccessTokenSerializer
) {
    companion object {
        /** The logger to use */
        private val LOG = LoggerFactory.getLogger(AuthenticationController::class.java)
    }

    /**
     * Start authentication with the given Authentication Provider
     * @param provider The provider to authenticate with
     * @return the redirect to the authentication provider
     */
    @RequestMapping(value = ["/login/{provider}/start"], method = [RequestMethod.POST])
    fun startAuthentication(@PathVariable("provider") provider: String): RedirectView {
        LOG.debug("Starting authentication with provider {}", provider)
        val externalAuthenticationProvider = registry[provider] !!

        val startAuthentication = externalAuthenticationProvider.startAuthentication()

        return RedirectView(startAuthentication.redirectUrl.toString())
    }

    /**
     * Complete authentication with the given Authentication Provider
     * @param provider The provider to authenticate with
     * @param params The parameters from the authentication provider
     */
    @RequestMapping(value = ["/login/{provider}/callback"], method = [RequestMethod.GET])
    @ResponseBody
    fun completeAuthentication(@PathVariable("provider") provider: String,
                               @RequestParam params: Map<String, String>,
                               response: HttpServletResponse) : RedirectView {
        LOG.debug("Completing authentication with provider {}: {}", provider, params)
        val externalAuthenticationProvider = registry[provider] !!

        val externalUser = externalAuthenticationProvider.completeAuthentication(params)

        var user = userService.findUserByProviderId(provider, externalUser.userId)
        if (user == null) {
            user = userService.create(UserData(
                    name = externalUser.name,
                    email = externalUser.email,
                    authenticationDetails = setOf(
                            AuthenticationDetails(
                                    provider = provider,
                                    providerId = externalUser.userId,
                                    displayName = externalUser.displayName
                            )
                    )
            ))
        }

        val accessToken = accessTokenBuilder.buildAccessToken(user)

        val authCookie = Cookie("nuworlds_access_token", accessTokenSerializer.serialize(accessToken))
        authCookie.isHttpOnly = true
        authCookie.path = "/"

        response.addCookie(authCookie)

        return RedirectView("/profile")
    }

    /**
     * Handle an error authenticating the user, displaying an appropriate page
     */
    @ExceptionHandler(ExternalProviderFailureException::class)
    fun authenticationError() = "authenticationError"
}
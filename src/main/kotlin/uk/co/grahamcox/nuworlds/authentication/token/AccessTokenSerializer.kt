package uk.co.grahamcox.nuworlds.authentication.token

interface AccessTokenSerializer {
    /**
     * Serialize the Access Token into a String
     * @param token The token to serialize
     * @return the serialized string
     */
    fun serialize(token: AccessToken) : String

    /**
     * Deserialize the given access token string
     * @param input The input to deserialize
     * @return the access token
     */
    fun deserialize(input: String) : AccessToken
}
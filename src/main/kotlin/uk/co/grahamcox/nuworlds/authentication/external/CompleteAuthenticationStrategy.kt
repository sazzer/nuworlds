package uk.co.grahamcox.nuworlds.authentication.external

/**
 * Strategy to complete the authentication process with an external authentication provider
 */
interface CompleteAuthenticationStrategy {
    /**
     * Complete authentication with the external authentication provider
     * @param params The parameters received from the external authentication provider
     * @return The external user that has authenticated
     */
    fun completeAuthentication(params: Map<String, String>) : ExternalUser
}
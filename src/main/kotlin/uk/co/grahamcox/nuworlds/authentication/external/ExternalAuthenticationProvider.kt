package uk.co.grahamcox.nuworlds.authentication.external

/**
 * Interface representing how we can authenticate via an external authentication provider
 */
interface ExternalAuthenticationProvider {
    /** Identity of this External Authentication Provider */
    val providerId: String

    /** Whether this provider is enabled */
    val enabled: Boolean

    /**
     * Start authentication with the external authentication provider
     */
    fun startAuthentication() : StartAuthenticationResult

    /**
     * Complete authentication with the external authentication provider
     * @param params The parameters received from the external authentication provider
     * @return The external user that has authenticated
     */
    fun completeAuthentication(params: Map<String, String>) : ExternalUser
}
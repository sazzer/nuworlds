package uk.co.grahamcox.nuworlds.authentication.token

import uk.co.grahamcox.nuworlds.users.UserId
import java.util.*

/**
 * Representation of an Access Token for the request
 * @property tokenId The ID of the token
 * @property userId The ID of the user the token is for
 */
data class AccessToken(
        val tokenId: UUID,
        val userId: UserId
)
package uk.co.grahamcox.nuworlds.authentication.external.google

import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Configuration
import org.springframework.context.support.GenericApplicationContext
import org.springframework.context.support.beans
import org.springframework.core.env.get
import org.springframework.web.client.RestOperations
import org.springframework.web.client.RestTemplate
import uk.co.grahamcox.nuworlds.authentication.external.ExternalAuthenticationProviderImpl
import java.net.URI

/**
 * Spring configuration for dealing with Authentication via Google
 */
@Configuration
class GoogleConfig(context: GenericApplicationContext) {
    companion object {
        /** The logger to use */
        private val LOG = LoggerFactory.getLogger(GoogleConfig::class.java)
    }

    init {
        beans {
            profile("!e2e") {
                bean("googleSettings") {
                    GoogleSettings(
                            clientId = env["externalAuth.google.clientId"] ?: "",
                            clientSecret = env["externalAuth.google.clientSecret"] ?: "",
                            callbackUrl = URI(env["externalAuth.google.callbackUrl"] ?: ""),
                            redirectUrl = URI(env["externalAuth.google.redirectUrl"]!!),
                            tokenUrl = URI(env["externalAuth.google.tokenUrl"]!!)
                    )
                }
            }

            bean("googleAuthenticationProvider") {
                val settings: GoogleSettings = ref("googleSettings")
                LOG.debug("Configuring Google Auth: {}", settings)

                ExternalAuthenticationProviderImpl("google_plus",
                        settings.configured,
                        GoogleStartAuthenticationStrategy(settings),
                        GoogleCompleteAuthenticationStrategy(settings, ref()))
            }
        }.initialize(context)
    }
}
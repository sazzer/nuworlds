package uk.co.grahamcox.nuworlds.authentication.external
/**
 * Standard implementation of the External Authentication Provider
 * @property providerId The ID of this Provider
 * @property startAuthenticationStrategy The strategy to use for starting authentication
 */
class ExternalAuthenticationProviderImpl(
        override val providerId: String,
        override val enabled: Boolean,
        private val startAuthenticationStrategy: StartAuthenticationStrategy,
        private val completeAuthenticationStrategy: CompleteAuthenticationStrategy
) : ExternalAuthenticationProvider {
    /**
     * Start authentication with the external authentication provider
     */
    override fun startAuthentication() = startAuthenticationStrategy.startAuthentication()

    /**
     * Complete authentication with the external authentication provider
     * @param params The parameters received from the external authentication provider
     * @return The external user that has authenticated
     */
    override fun completeAuthentication(params: Map<String, String>) = completeAuthenticationStrategy.completeAuthentication(params)
}
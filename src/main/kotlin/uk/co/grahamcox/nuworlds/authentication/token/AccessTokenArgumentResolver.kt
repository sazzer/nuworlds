package uk.co.grahamcox.nuworlds.authentication.token

import org.springframework.core.MethodParameter
import org.springframework.web.bind.support.WebDataBinderFactory
import org.springframework.web.context.request.NativeWebRequest
import org.springframework.web.context.request.ServletWebRequest
import org.springframework.web.method.support.HandlerMethodArgumentResolver
import org.springframework.web.method.support.ModelAndViewContainer
import uk.co.grahamcox.nuworlds.users.UserId
import kotlin.reflect.jvm.kotlinFunction

/**
 * Argument Resolver for providing an Access Token or User ID from the current request
 */
class AccessTokenArgumentResolver(
        private val accessTokenSerializer: AccessTokenSerializer
) : HandlerMethodArgumentResolver {
    /**
     * Check whether we can support the requested parameter
     */
    override fun supportsParameter(parameter: MethodParameter) =
            parameter.parameterType == AccessToken::class.java
                    || parameter.parameterType == UserId::class.java

    /**
     * Resolve the argument for this request
     */
    override fun resolveArgument(parameter: MethodParameter, mvc: ModelAndViewContainer?,
                                 request: NativeWebRequest,
                                 dataBinderFactory: WebDataBinderFactory?): Any? {
        val parameterIndex = parameter.parameterIndex
        val nullable = parameter.method?.kotlinFunction!!.parameters[parameterIndex + 1].type.isMarkedNullable

        val servletRequest = request as ServletWebRequest
        val accessToken = servletRequest.request.cookies?.firstOrNull { it.name == "nuworlds_access_token" }
                ?.value
                ?.let { accessTokenSerializer.deserialize(it) }

        if (!nullable && accessToken == null) {
            throw MissingAccessTokenException()
        }

        return when(parameter.parameterType) {
            AccessToken::class.java -> accessToken
            UserId::class.java -> accessToken?.userId
            else -> null
        }
    }
}
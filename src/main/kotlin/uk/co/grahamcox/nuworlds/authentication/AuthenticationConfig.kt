package uk.co.grahamcox.nuworlds.authentication

import com.auth0.jwt.algorithms.Algorithm
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.context.support.GenericApplicationContext
import org.springframework.context.support.beans
import uk.co.grahamcox.nuworlds.authentication.external.ExternalAuthenticationProvider
import uk.co.grahamcox.nuworlds.authentication.external.ExternalAuthenticationProviderRegistry
import uk.co.grahamcox.nuworlds.authentication.external.google.GoogleConfig
import uk.co.grahamcox.nuworlds.authentication.token.AccessTokenArgumentResolver
import uk.co.grahamcox.nuworlds.authentication.token.AccessTokenBuilder
import uk.co.grahamcox.nuworlds.authentication.token.JwtAccessTokenSerializerImpl

/**
 * Spring configuration for dealing with Authentication
 */
@Configuration
@Import(
        GoogleConfig::class
)
class AuthenticationConfig(context: GenericApplicationContext) {
    init {
        beans {
            bean {
                val providers = context.getBeansOfType(ExternalAuthenticationProvider::class.java)
                        .values
                        .filter { it.enabled }
                        .toList()

                ExternalAuthenticationProviderRegistry(providers)
            }

            bean<AccessTokenBuilder>()
            bean {
                JwtAccessTokenSerializerImpl(Algorithm.HMAC512("superSecretKey"))
            }
            bean<AuthenticationControllerAdvice>()
            bean<AuthenticationController>()
            bean<AccessTokenArgumentResolver>()
        }.initialize(context)
    }
}
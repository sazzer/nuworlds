package uk.co.grahamcox.nuworlds.authentication.external

/**
 * Details of the user as represented by the external authentication provider
 * @property userId The ID of the user at this external authentication provider
 * @property displayName The display name of the user at this external authentication provider
 * @property name The name of the user
 * @property email The email address of the user
 */
data class ExternalUser(
        val userId: String,
        val displayName: String,
        val name: String,
        val email: String
)
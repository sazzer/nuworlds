package uk.co.grahamcox.nuworlds.authentication.token

import java.lang.Exception

/**
 * Exception to indicate that a request that required an Access Token didn't have access to one
 */
class MissingAccessTokenException : Exception()
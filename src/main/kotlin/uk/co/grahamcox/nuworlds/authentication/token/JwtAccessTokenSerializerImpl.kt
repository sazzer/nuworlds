package uk.co.grahamcox.nuworlds.authentication.token

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import com.auth0.jwt.exceptions.JWTDecodeException
import org.slf4j.LoggerFactory
import uk.co.grahamcox.nuworlds.users.UserId
import java.lang.IllegalArgumentException
import java.util.*

/**
 * Means to serialize an Access Token into a String and back
 */
class JwtAccessTokenSerializerImpl(private val signingAlgorithm: Algorithm) : AccessTokenSerializer {
    companion object {
        /** The logger to use */
        private val LOG = LoggerFactory.getLogger(JwtAccessTokenSerializerImpl::class.java)
    }

    /**
     * Serialize the Access Token into a String
     * @param token The token to serialize
     * @return the serialized string
     */
    override fun serialize(token: AccessToken) : String {
        LOG.trace("Serializing token {}", token)
        val serialized = JWT.create()
                .withSubject(token.userId.id.toString())
                .withJWTId(token.tokenId.toString())
                .sign(signingAlgorithm)

        LOG.trace("Serialized token {} as {}", token, serialized)
        return serialized
    }

    /**
     * Deserialize the given access token string
     * @param input The input to deserialize
     * @return the access token
     */
    override fun deserialize(input: String) : AccessToken {
        LOG.trace("Deserializing token {}", input)
        val decodedJWT = try {
            JWT.require(signingAlgorithm).build().verify(input)
        } catch (e: JWTDecodeException) {
            LOG.warn("Failed to parse access token: {}", input, e)
            throw InvalidAccessTokenException("Failed to parse access token")
        }

        val token = try {
            AccessToken(
                    userId = UserId(UUID.fromString(decodedJWT.subject ?: throw InvalidAccessTokenException("No Subject in Access Token"))),
                    tokenId = UUID.fromString(decodedJWT.id ?: throw InvalidAccessTokenException("No ID in Access Token"))
            )
        } catch (e: IllegalArgumentException) {
            LOG.warn("Failed to parse access token: {}", input, e)
            throw InvalidAccessTokenException("Failed to parse access token")
        }

        LOG.trace("Deserialized token {} as {}", input, token)
        return token
    }
}
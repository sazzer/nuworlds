package uk.co.grahamcox.nuworlds.authentication.external

/**
 * Exception to indicate that a call to an external provider failed in an unexpected way
 */
class ExternalProviderFailureException(message: String) : Exception(message)
package uk.co.grahamcox.nuworlds.authentication.external

/**
 * Strategy to start the authentication process with an external authentication provider
 */
interface StartAuthenticationStrategy {
    /**
     * Start authentication with the external authentication provider
     */
    fun startAuthentication(): StartAuthenticationResult;
}
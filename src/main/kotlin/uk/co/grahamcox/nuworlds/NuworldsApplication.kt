package uk.co.grahamcox.nuworlds

import org.springframework.boot.SpringBootConfiguration
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.runApplication
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.context.annotation.Import
import org.springframework.context.support.GenericApplicationContext
import org.springframework.context.support.beans
import org.springframework.web.client.RestTemplate
import uk.co.grahamcox.nuworlds.authentication.AuthenticationConfig
import uk.co.grahamcox.nuworlds.users.spring.UserConfig
import uk.co.grahamcox.nuworlds.web.WebConfig
import java.time.Clock

/**
 * The core application context for the entire application
 */
@SpringBootConfiguration
@EnableAutoConfiguration
@Import(
		AuthenticationConfig::class,
		UserConfig::class,
		WebConfig::class
)
class NuworldsApplication(context: GenericApplicationContext) {
	init {
		beans {
			bean {
				Clock.systemUTC()
			}
			bean {
				RestTemplateBuilder().build()
			}
			bean<HomePageController>()
		}.initialize(context)
	}
}

/**
 * Run the application
 */
fun main(args: Array<String>) {
	runApplication<NuworldsApplication>(*args)
}

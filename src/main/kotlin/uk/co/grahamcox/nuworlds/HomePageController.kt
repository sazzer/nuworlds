package uk.co.grahamcox.nuworlds

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod

/**
 * Controller for rendering the home page
 */
@Controller
@RequestMapping("/")
class HomePageController {
    /**
     * Generate the home page
     */
    @RequestMapping(method = [RequestMethod.GET])
    fun homePage() = "home"
}
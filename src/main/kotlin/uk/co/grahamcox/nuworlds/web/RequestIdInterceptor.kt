package uk.co.grahamcox.nuworlds.web

import org.slf4j.LoggerFactory
import org.slf4j.MDC
import org.springframework.web.servlet.HandlerInterceptor
import java.lang.Exception
import java.util.*
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * Interceptor to generate a unique ID for each request
 */
class RequestIdInterceptor : HandlerInterceptor {
    companion object {
        /** The header to put the Request ID into */
        private const val REQUEST_ID_HEADER = "X-Request-ID"

        /** The MDC Key in which to put the Request ID */
        private const val REQUEST_ID_MDC_KEY = "RequestID"
    }

    init {
        LoggerFactory.getLogger(RequestIdInterceptor::class.java).warn("Starting")
    }

    /**
     * Generate the ID and store it into the SLF4J MDC, as well as adding it to the response
     */
    override fun preHandle(request: HttpServletRequest, response: HttpServletResponse, handler: Any): Boolean {
        val requestId = request.getHeader(REQUEST_ID_HEADER) ?: UUID.randomUUID().toString()

        MDC.put(REQUEST_ID_MDC_KEY, requestId)

        response.addHeader(REQUEST_ID_HEADER, requestId)

        return true
    }

    /**
     * Clear the Request ID from the MDC
     */
    override fun afterCompletion(request: HttpServletRequest, response: HttpServletResponse, handler: Any, ex: Exception?) {
        MDC.remove(REQUEST_ID_MDC_KEY)
    }

}
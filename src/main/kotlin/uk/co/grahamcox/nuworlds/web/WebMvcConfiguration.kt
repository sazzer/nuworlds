package uk.co.grahamcox.nuworlds.web

import org.springframework.context.annotation.Configuration
import org.springframework.web.method.support.HandlerMethodArgumentResolver
import org.springframework.web.servlet.HandlerInterceptor
import org.springframework.web.servlet.config.annotation.InterceptorRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer

/**
 * Configuration for the Web MVC system
 */
@Configuration
class WebMvcConfiguration(
        private val interceptors: List<HandlerInterceptor>,
        private val argumentResolvers: List<HandlerMethodArgumentResolver>
) : WebMvcConfigurer {
    /**
     * Add our interceptors to the Interceptor Registry
     */
    override fun addInterceptors(registry: InterceptorRegistry) {
        interceptors.forEach { registry.addInterceptor(it) }
    }

    /**
     * Add our Argument Resolvers to the registry
     */
    override fun addArgumentResolvers(resolvers: MutableList<HandlerMethodArgumentResolver>) {
        resolvers.addAll(argumentResolvers)
    }
}
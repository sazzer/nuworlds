package uk.co.grahamcox.nuworlds.web

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import

/**
 * Spring Config for the general Webapp
 */
@Configuration
@Import(WebMvcConfiguration::class)
class WebConfig {
    @Bean
    fun requestIdInterceptor() = RequestIdInterceptor()
}
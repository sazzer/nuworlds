package uk.co.grahamcox.nuworlds.dao

import java.sql.ResultSet
import java.util.*

/**
 * Get a UUID Column from the ResultSet
 * @param name The name of the column
 * @return the UUID value
 */
fun ResultSet.getUUID(name: String) = UUID.fromString(this.getString(name))

/**
 * Get an Instant Column from the ResultSet
 * @param name The name of the column
 * @return the Instant value
 */
fun ResultSet.getInstant(name: String) = this.getTimestamp(name).toInstant()
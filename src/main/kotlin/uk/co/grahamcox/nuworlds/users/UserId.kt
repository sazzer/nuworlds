package uk.co.grahamcox.nuworlds.users

import uk.co.grahamcox.nuworlds.model.Id
import java.util.*

/**
 * ID of a User
 * @property id The actual ID
 */
data class UserId(override val id: UUID) : Id
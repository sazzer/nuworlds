package uk.co.grahamcox.nuworlds.users

/**
 * Authentication Details to authenticate a user with a particular external authentication provider
 * @property provider The provider to authenticate with
 * @property providerId The ID of the user at this provider
 * @property displayName The provider-specific display name of these authentication details
 */
data class AuthenticationDetails(
        val provider: String,
        val providerId: String,
        val displayName: String
) : Comparable<AuthenticationDetails> {
    /**
     * Compares this object with the specified object for order. Returns zero if this object is equal
     * to the specified [other] object, a negative number if it's less than [other], or a positive number
     * if it's greater than [other].
     */
    override fun compareTo(other: AuthenticationDetails) = compareValuesBy(this, other,
            {it.provider},
            {it.displayName},
            {it.providerId}
    )
}
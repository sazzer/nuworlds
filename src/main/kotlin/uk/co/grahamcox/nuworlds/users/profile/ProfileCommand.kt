package uk.co.grahamcox.nuworlds.users.profile

import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

/**
 * Command object to represent the user profile details being saved
 * @property name The new name of the user
 * @property email The new email address of the user
 */
data class ProfileCommand(
        @field:NotNull @field:Size(min = 1)
        val name: String?,

        @field:NotNull @field:Size(min = 1)
        val email: String?
)
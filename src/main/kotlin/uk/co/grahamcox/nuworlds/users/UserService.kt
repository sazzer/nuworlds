package uk.co.grahamcox.nuworlds.users

/**
 * Service for interacting with users
 */
interface UserService {
    /**
     * Get a single User by their unique ID
     * @param id The ID of the user
     * @return the user
     */
    fun getById(id: UserId) : UserModel

    /**
     * Create a new user record from the provided data
     * @param data The data for the user
     * @return the newly created user
     */
    fun create(data: UserData): UserModel

    /**
     * Attempt to find the user with the given ID at the given Provider
     * @param provider The name of the provider
     * @param providerId The ID of the user at this provider
     * @return the user
     */
    fun findUserByProviderId(provider: String, providerId: String) : UserModel?

    /**
     * Attempt to find and update the user with the given ID
     * @param userId The ID of the user to update
     * @param updater A lambda to update the data with
     * @return the newly updated user
     */
    fun updateUser(userId: UserId, updater: (UserData) -> UserData): UserModel
}
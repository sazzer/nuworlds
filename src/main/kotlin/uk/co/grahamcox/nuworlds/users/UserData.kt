package uk.co.grahamcox.nuworlds.users

/**
 * Details of a user
 * @property name The name of the user
 * @property email The email address of the user
 * @property authenticationDetails The authentication details for this user
 */
data class UserData(
        val name: String,
        val email: String,
        val authenticationDetails: Collection<AuthenticationDetails>
)
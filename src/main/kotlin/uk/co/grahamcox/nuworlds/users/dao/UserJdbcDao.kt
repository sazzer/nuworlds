package uk.co.grahamcox.nuworlds.users.dao

import com.fasterxml.jackson.databind.ObjectMapper
import org.slf4j.LoggerFactory
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations
import uk.co.grahamcox.nuworlds.dao.getInstant
import uk.co.grahamcox.nuworlds.dao.getUUID
import uk.co.grahamcox.nuworlds.model.Identity
import uk.co.grahamcox.nuworlds.users.*
import java.lang.UnsupportedOperationException
import java.sql.ResultSet
import java.time.Clock
import java.util.*

/**
 * DAO for working with Users via JDBC
 * @property jdbcOperations The means to interact with the database
 * @property objectMapper The object mapper to work with JSON
 * @property clock The clock to use
 */
class UserJdbcDao(
        private val jdbcOperations: NamedParameterJdbcOperations,
        private val objectMapper: ObjectMapper,
        private val clock: Clock) : UserService {
    companion object {
        /** The logger to use */
        private val LOG = LoggerFactory.getLogger(UserJdbcDao::class.java)
    }

    /**
     * Get a single User by their unique ID
     * @param id The ID of the user
     * @return the user
     */
    override fun getById(id: UserId) : UserModel {
        LOG.debug("Loading user by ID {}", id)
        val user = try {
            jdbcOperations.queryForObject("SELECT * FROM users WHERE user_id = :userId",
                    mapOf("userId" to id.id), this::mapRecord)!!
        } catch (e: EmptyResultDataAccessException) {
            LOG.warn("Attempted to load an unknown user by ID: {}", id, e)
            throw UnknownUserException(id)
        }

        LOG.debug("Loaded user: {}", user)
        return user
    }

    /**
     * Attempt to find the user with the given ID at the given Provider
     * @param provider The name of the provider
     * @param providerId The ID of the user at this provider
     * @return the user
     */
    override fun findUserByProviderId(provider: String, providerId: String) : UserModel? {
        LOG.debug("Loading user by ID {} at provider {}", providerId, provider)
        val match = mapOf(
                "provider" to provider,
                "providerId" to providerId
        )

        val user = try {
            jdbcOperations.queryForObject("SELECT * FROM users WHERE authentication @> :authentication::jsonb",
                    mapOf("authentication" to objectMapper.writeValueAsString(listOf(match))), this::mapRecord)!!
        } catch (e: EmptyResultDataAccessException) {
            LOG.info("No user found for ID {} at provider {}", providerId, provider)
            null
        }

        LOG.debug("Loaded user: {}", user)
        return user
    }

    /**
     * Create a new user record from the provided data
     * @param data The data for the user
     * @return the newly created user
     */
    override fun create(data: UserData): UserModel {
        LOG.debug("Creating user: {}", data)
        val user =
            jdbcOperations.queryForObject("""
                INSERT INTO users(user_id, version, created, updated, name, email, authentication)
                VALUES (:userId, :version, :created, :updated, :name, :email, :authentication::jsonb)
                RETURNING *
            """, mapOf(
                    "userId" to UUID.randomUUID(),
                    "version" to UUID.randomUUID(),
                    "created" to Date.from(clock.instant()),
                    "updated" to Date.from(clock.instant()),
                    "name" to data.name,
                    "email" to data.email,
                    "authentication" to objectMapper.writeValueAsString(data.authenticationDetails)
            ), this::mapRecord)!!

        LOG.debug("Created user: {}", user)
        return user
    }

    /**
     * Attempt to find and update the user with the given ID
     * @param userId The ID of the user to update
     * @param updater A lambda to update the data with
     * @return the newly updated user
     */
    override fun updateUser(userId: UserId, updater: (UserData) -> UserData): UserModel {
        LOG.debug("Updating user: {}", userId)

        val current = getById(userId)
        val updated = updater(current.data)

        val user =
                jdbcOperations.queryForObject("""
                UPDATE users 
                SET version = :version, updated = :updated, name = :name, email = :email, authentication = :authentication::jsonb
                WHERE user_id = :userId
                RETURNING *
            """, mapOf(
                        "userId" to userId.id,
                        "version" to UUID.randomUUID(),
                        "updated" to Date.from(clock.instant()),
                        "name" to updated.name,
                        "email" to updated.email,
                        "authentication" to objectMapper.writeValueAsString(updated.authenticationDetails)
                ), this::mapRecord)!!

        LOG.debug("Updated user: {}", user)
        return user
    }

    /**
     * Map a single row in the database in to a User object
     * @param rs The resultset to get the data from
     * @return the user model
     */
    private fun mapRecord(rs: ResultSet, _row: Int) : UserModel {
        val authDetails = rs.getString("authentication")
        val authDetailsType = objectMapper.typeFactory.constructCollectionType(Set::class.java, AuthenticationDetails::class.java)
        val parsedAuthDetails = objectMapper.readValue<Set<AuthenticationDetails>>(authDetails, authDetailsType)

        return UserModel(
                identity = Identity(
                        id = UserId(rs.getUUID("user_id")),
                        version = rs.getUUID("version"),
                        created = rs.getInstant("created"),
                        updated = rs.getInstant("updated")
                ),
                data = UserData(
                        name = rs.getString("name"),
                        email = rs.getString("email"),
                        authenticationDetails = parsedAuthDetails.sorted()
                )
        )
    }
}
package uk.co.grahamcox.nuworlds.users.profile

import org.springframework.http.HttpStatus
import org.springframework.stereotype.Controller
import org.springframework.validation.BindException
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.ModelAndView
import org.springframework.web.servlet.view.RedirectView
import uk.co.grahamcox.nuworlds.authentication.token.AccessToken
import uk.co.grahamcox.nuworlds.users.UserModel
import uk.co.grahamcox.nuworlds.users.UserService
import javax.validation.Valid

/**
 * Controller for working with user profiles
 */
@Controller
@RequestMapping("/profile")
class ProfileController(private val userService: UserService) {
    /**
     * Show the user profile
     * @param accessToken The Access Token, to prove that we're logged in
     * @return the view
     */
    @RequestMapping(method = [RequestMethod.GET])
    fun showProfile(accessToken: AccessToken,
                    @ModelAttribute("currentUser") userModel: UserModel,
                    @RequestParam(value = "success", required = false) success: String?): ModelAndView {
        return ModelAndView("profile/main",
                mapOf(
                        "saved" to (success != null),
                        "userDetails" to ProfileCommand(
                                name = userModel.data.name,
                                email = userModel.data.email
                        )
                ))
    }

    /**
     * Update the user profile
     * @param accessToken The Access Token, to prove that we're logged in
     * @return A redirect back to the profile
     */
    @RequestMapping(method = [RequestMethod.POST])
    fun saveProfile(accessToken: AccessToken, @Valid updatedProfile: ProfileCommand, bindingResult: BindingResult): ModelAndView {
        if (bindingResult.hasErrors()) {
            val fieldErrors = bindingResult.fieldErrors.map { error ->
                error.field to error.code
            }.toMap()

            val modelAndView = ModelAndView("profile/main",
                    mapOf(
                            "fieldErrors" to fieldErrors,
                            "userDetails" to updatedProfile
                    )
            )
            modelAndView.status = HttpStatus.BAD_REQUEST
            return modelAndView
        }

        userService.updateUser(accessToken.userId) { user ->
            user.copy(
                    name = updatedProfile.name!!.trim(),
                    email = updatedProfile.email!!.trim()
            )
        }

        return ModelAndView(RedirectView("/profile?success"))
    }

    /**
     * Show the authentication providers for the user
     * @param accessToken The Access Token, to prove that we're logged in
     * @return the view
     */
    @RequestMapping(value = ["/providers"], method = [RequestMethod.GET])
    fun showProviders(accessToken: AccessToken): String {
        return "profile/providers"
    }
}
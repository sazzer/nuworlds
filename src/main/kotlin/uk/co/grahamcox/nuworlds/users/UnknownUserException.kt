package uk.co.grahamcox.nuworlds.users

/**
 * Exception to indicate that a User ID doesn't exist
 * @property id The ID of the user
 */
class UnknownUserException(val id: UserId) : Exception("Unknown user: $id")
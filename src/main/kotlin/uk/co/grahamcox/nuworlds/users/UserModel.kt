package uk.co.grahamcox.nuworlds.users

import uk.co.grahamcox.nuworlds.model.Model

/**
 * Type Alias for a User Model
 */
typealias UserModel = Model<UserId, UserData>
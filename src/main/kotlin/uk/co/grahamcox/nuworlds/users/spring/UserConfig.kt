package uk.co.grahamcox.nuworlds.users.spring

import org.springframework.context.annotation.Configuration
import org.springframework.context.support.GenericApplicationContext
import org.springframework.context.support.beans
import uk.co.grahamcox.nuworlds.users.dao.UserJdbcDao
import uk.co.grahamcox.nuworlds.users.profile.ProfileController
import uk.co.grahamcox.nuworlds.users.web.UserControllerAdvice

/**
 * Spring configuration for working with Users
 */
@Configuration
class UserConfig(context: GenericApplicationContext) {
    init {
        beans {
            bean<UserJdbcDao>()

            bean<ProfileController>()
            bean<UserControllerAdvice>()
        }.initialize(context)
    }
}
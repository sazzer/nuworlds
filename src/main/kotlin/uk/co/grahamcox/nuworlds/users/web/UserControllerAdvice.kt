package uk.co.grahamcox.nuworlds.users.web

import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ModelAttribute
import uk.co.grahamcox.nuworlds.authentication.token.MissingAccessTokenException
import uk.co.grahamcox.nuworlds.model.Model
import uk.co.grahamcox.nuworlds.users.*

/**
 * Controller Advice with user based details
 */
@ControllerAdvice
class UserControllerAdvice(
        private val userService: UserService
) {
    /**
     * Load the current user and make it available to the views
     * @param userId The ID of the current user
     * @return the current user
     */
    @ModelAttribute("currentUser")
    fun getCurrentUser(userId: UserId?): UserModel? {
        try {
            return userId?.let(userService::getById)
        } catch (e: UnknownUserException) {
            // If the authenticated User ID doesn't exist then act as if the access token is invalid
            throw MissingAccessTokenException()
        }
    }
}
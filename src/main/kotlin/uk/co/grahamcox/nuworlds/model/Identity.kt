package uk.co.grahamcox.nuworlds.model

import java.time.Instant
import java.util.*

/**
 * The identity of some model that has been loaded from the data store
 * @property id The ID of the model
 * @property version The version of the model
 * @property created When the model was first created
 * @property updated When the model was last updated
 */
data class Identity<ID>(
        val id: ID,
        val version: UUID,
        val created: Instant,
        val updated: Instant
)
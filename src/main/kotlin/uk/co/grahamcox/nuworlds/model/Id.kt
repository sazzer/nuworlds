package uk.co.grahamcox.nuworlds.model

import java.util.*

/**
 * Interface representing the ID of some model
 */
interface Id {
    val id: UUID
}
package uk.co.grahamcox.nuworlds.model

/**
 * A Model that has been loaded from the data store
 * @property identity The identity of the model
 * @property data The actual model data
 */
data class Model<ID, DATA>(val identity: Identity<ID>, val data: DATA)
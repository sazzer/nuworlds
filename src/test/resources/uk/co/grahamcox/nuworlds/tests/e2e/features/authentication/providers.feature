Feature: Authentication Providers

  Scenario: The correct authentication providers are offered
    When I load the home page
    Then I can authenticate using:
      | google_plus |
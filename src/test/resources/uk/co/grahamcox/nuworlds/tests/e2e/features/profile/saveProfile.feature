Feature: Update User Profile

  Background:
    Given a user exists with details:
      | User ID                | 2BCE54F8-0D7E-4721-8863-C7E351E00D39                                                     |
      | Name                   | My Test User                                                                             |
      | Email Address          | my-test@example.com                                                                      |
      | Authentication Details | google_plus;testuserid-1234567890;test@example.com,twitter;testuserid-1234567890;@mytest |
    And I load the home page
    And I authenticate using "google_plus"
    And I view the user profile page

  Scenario: Update the user profile with valid details
    When I update the user profile with details:
      | Name          | New User        |
      | Email Address | new@example.com |
    Then the user profile is saved successfully
    And the user profile has values:
      | Name          | New User        |
      | Email Address | new@example.com |

  Scenario: Update the user profile with no name
    When I update the user profile with details:
      | Name |  |
    Then the user profile saved with errors:
      | Please enter your name |
    And the user profile has values:
      | Name          |                     |
      | Email Address | my-test@example.com |

  Scenario: Update the user profile with no email
    When I update the user profile with details:
      | Email Address |  |
    Then the user profile saved with errors:
      | Please enter your Email Address |
    And the user profile has values:
      | Name          | My Test User |
      | Email Address |              |

  Scenario: Update the user profile with no name or email
    When I update the user profile with details:
      | Name          |  |
      | Email Address |  |
    Then the user profile saved with errors:
      | Please enter your name          |
      | Please enter your Email Address |
    And the user profile has values:
      | Name          |  |
      | Email Address |  |
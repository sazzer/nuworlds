Feature: View User Profile

  Background:
    Given a user exists with details:
      | User ID                | 2BCE54F8-0D7E-4721-8863-C7E351E00D39                                                     |
      | Name                   | My Test User                                                                             |
      | Email Address          | my-test@example.com                                                                      |
      | Authentication Details | google_plus;testuserid-1234567890;test@example.com,twitter;testuserid-1234567890;@mytest |
    And I load the home page
    And I authenticate using "google_plus"

  Scenario: View Profile Page when Authenticated
    When I view the user profile page
    Then the user profile has values:
      | Name          | My Test User        |
      | Email Address | my-test@example.com |

  Scenario: View Providers List when Authenticated
    When I view the user profile page
    And I view the authentication providers list on the user profile page
    Then the user profile has authentication provider:
      | Provider     | Google           |
      | Display Name | test@example.com |
    And the user profile has authentication provider:
      | Provider     | Twitter |
      | Display Name | @mytest |

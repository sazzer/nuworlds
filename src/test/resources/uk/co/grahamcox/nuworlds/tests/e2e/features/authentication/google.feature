Feature: Authentication via Google

  Scenario: Authenticating as a new user via Google
    Given I load the home page
    When I authenticate using "google_plus"
    Then I am logged in as "Test User"
    And there is a user in the database matching:
      | Name          | Test User        |
      | Email Address | test@example.com |

  Scenario: Authenticating as an existing user via Google
    Given a user exists with details:
      | User ID                | 2bce54f8-0d7e-4721-8863-c7e351e00d39               |
      | Name                   | My Test User                                       |
      | Email Address          | my-test@example.com                                |
      | Authentication Details | google_plus;testuserid-1234567890;test@example.com |
    And I load the home page
    When I authenticate using "google_plus"
    Then I am logged in as "My Test User"
    And there is a user in the database matching:
      | User ID       | 2bce54f8-0d7e-4721-8863-c7e351e00d39 |
      | Name          | My Test User                         |
      | Email Address | my-test@example.com                  |

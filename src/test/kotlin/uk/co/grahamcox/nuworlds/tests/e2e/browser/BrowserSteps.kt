package uk.co.grahamcox.nuworlds.tests.e2e.browser

import io.cucumber.java8.En
import org.springframework.beans.factory.annotation.Autowired

/**
 * Cucumber setup for ensuring the web browser is in the correct state
 */
class BrowserSteps : En {
    /** The Web Driver Wrapper to use */
    @Autowired
    private lateinit var webDriverWrapper: WebDriverWrapper

    init {
        Before { scenario ->
            webDriverWrapper.start()
        }

        After { scenario ->
            webDriverWrapper.record(scenario.name)
            webDriverWrapper.stop()
        }
    }
}
package uk.co.grahamcox.nuworlds.tests.integration

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.testcontainers.containers.Network
import uk.co.grahamcox.nuworlds.tests.database.DatabaseCleaner
import uk.co.grahamcox.nuworlds.tests.database.DatabaseConfig
import uk.co.grahamcox.nuworlds.tests.e2e.database.DatabaseSeeder
import java.time.Instant
import javax.sql.DataSource

/**
 * Spring configuration for the integration tests
 */
@Configuration
@Import(
        DatabaseConfig::class
)
class IntegrationTestConfig {
    @Bean
    fun network() = Network.builder()
            .createNetworkCmdModifier {
                it.withName("nuworlds-test-${Instant.now()}")
            }
            .build()

    @Bean
    fun databaseCleaner(dataSource: DataSource) = DatabaseCleaner(
            dataSource,
            listOf(
                    "flyway_schema_history"
            )
    )

    @Bean
    fun databaseSeeder() = DatabaseSeeder()
}
package uk.co.grahamcox.nuworlds.tests.e2e.users.profile

import org.openqa.selenium.WebElement
import org.openqa.selenium.support.FindBy
import org.openqa.selenium.support.PageFactory
import org.openqa.selenium.support.pagefactory.DefaultElementLocatorFactory

/**
 * Representation of a single Authentication Provider in the list
 */
class ProviderEntry(base: WebElement) {
    init {
        PageFactory.initElements(DefaultElementLocatorFactory(base), this)
    }

    /** The web element representing the display name */
    @FindBy(css = ".content .header")
    private lateinit var displayNameElement: WebElement

    /** The web element representing the provider name */
    @FindBy(css = ".content .description")
    private lateinit var providerNameElement: WebElement

    /** The display name */
    val displayName: String
        get() = displayNameElement.text

    /** The provider name */
    val providerName: String
        get() = providerNameElement.text
}
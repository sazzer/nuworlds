package uk.co.grahamcox.nuworlds.tests.integration.profile

import ch.tutteli.atrium.api.cc.en_GB.contains
import ch.tutteli.atrium.api.cc.en_GB.containsKey
import ch.tutteli.atrium.api.cc.en_GB.returnValueOf
import ch.tutteli.atrium.api.cc.en_GB.toBe
import ch.tutteli.atrium.verbs.expect
import com.karumi.kotlinsnapshot.matchWithSnapshot
import org.jsoup.Jsoup
import org.junit.jupiter.api.Test
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.web.util.UriComponentsBuilder

/**
 * Integration Tests for saving the Profile Page
 */
class SaveProfilePageTest : ProfilePageTestBase() {
    @Test
    fun saveProfilePageAllBlank() {
        val response = mvc.perform(MockMvcRequestBuilders.post("/profile")
                .param("name", "")
                .param("email", "")
                .cookie(authCookie))
                .andExpect(MockMvcResultMatchers.status().isBadRequest)
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith("text/html"))
                .andExpect(MockMvcResultMatchers.view().name("profile/main"))
                .andReturn()
                .response

        val parsed = Jsoup.parse(response.contentAsString)
        expect(parsed.select("""div.required.field:has(input[name="name"])""")) {
            returnValueOf(subject::hasClass, "error").toBe(true)
        }
        expect(parsed.select("""div.required.field:has(input[name="email"])""")) {
            returnValueOf(subject::hasClass, "error").toBe(true)
        }
        parsed.normalise().html().matchWithSnapshot()
    }

    @Test
    fun saveProfilePageNameBlank() {
        val response = mvc.perform(MockMvcRequestBuilders.post("/profile")
                .param("name", "")
                .param("email", "test@example.com")
                .cookie(authCookie))
                .andExpect(MockMvcResultMatchers.status().isBadRequest)
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith("text/html"))
                .andExpect(MockMvcResultMatchers.view().name("profile/main"))
                .andReturn()
                .response

        val parsed = Jsoup.parse(response.contentAsString)
        expect(parsed.select("""div.required.field:has(input[name="name"])""")) {
            returnValueOf(subject::hasClass, "error").toBe(true)
        }
        expect(parsed.select("""div.required.field:has(input[name="email"])""")) {
            returnValueOf(subject::hasClass, "error").toBe(false)
        }
        parsed.normalise().html().matchWithSnapshot()
    }

    @Test
    fun saveProfilePageEmailBlank() {
        val response = mvc.perform(MockMvcRequestBuilders.post("/profile")
                .param("name", "New User")
                .param("email", "")
                .cookie(authCookie))
                .andExpect(MockMvcResultMatchers.status().isBadRequest)
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith("text/html"))
                .andExpect(MockMvcResultMatchers.view().name("profile/main"))
                .andReturn()
                .response

        val parsed = Jsoup.parse(response.contentAsString)
        expect(parsed.select("""div.required.field:has(input[name="name"])""")) {
            returnValueOf(subject::hasClass, "error").toBe(false)
        }
        expect(parsed.select("""div.required.field:has(input[name="email"])""")) {
            returnValueOf(subject::hasClass, "error").toBe(true)
        }
        parsed.normalise().html().matchWithSnapshot()
    }

    @Test
    fun saveProfilePageSuccess() {
        val response = mvc.perform(MockMvcRequestBuilders.post("/profile")
                .param("name", "New User")
                .param("email", "new@example.com")
                .cookie(authCookie))
                .andExpect(MockMvcResultMatchers.status().isFound)
                .andExpect(MockMvcResultMatchers.header().exists("Location"))
                .andReturn()
                .response

        val locationHeader = response.getHeader("Location")!!

        val uriComponents = UriComponentsBuilder.fromUriString(locationHeader).build()

        expect(uriComponents) {
            returnValueOf(subject::getPath).toBe("/profile")
            returnValueOf(subject::getQueryParams).contains("success" to listOf(null))
        }

    }
}
package uk.co.grahamcox.nuworlds.tests.e2e.pages

import org.openqa.selenium.WebDriver
import org.slf4j.LoggerFactory
import uk.co.grahamcox.nuworlds.tests.e2e.browser.WebDriverWrapper
import kotlin.reflect.KFunction1
import kotlin.reflect.jvm.javaConstructor

/**
 * The mechanism to load pages in the web driver
 */
class PageLoader(private val serverPort: Integer, private val wrapper: WebDriverWrapper) {
    companion object {
        /** The logger to use */
        private val LOG = LoggerFactory.getLogger(PageLoader::class.java)
    }

    /**
     * Direct the web driver to get the requested page
     * @param url The URL to get on the webapp
     * @param constructor The page model constructor
     * @return the page model
     */
    fun <T> navigateTo(url: String, constructor: KFunction1<WebDriver, T>): T {
        val targetUrl = "http://host.testcontainers.internal:$serverPort/$url"
        LOG.debug("Opening URL: {}", targetUrl)

        wrapper.webDriver.get(targetUrl)

        return getPageModel(constructor)
    }

    /**
     * Build the page model for the given type
     * @param constructor The page model constructor
     * @return the page model
     */
    fun <T> getPageModel(constructor: KFunction1<WebDriver, T>) = constructor(wrapper.webDriver)

    /**
     * Direct the web driver to get the requested page
     * @param constructor The page model constructor
     * @return the page model
     */
    fun <T> navigateTo(constructor: KFunction1<WebDriver, T>): T {
        val url = constructor.javaConstructor!!.declaringClass.annotations
                .filterIsInstance<PageLink>()
                .map(PageLink::url)
                .firstOrNull()
                ?: throw IllegalArgumentException("Cannot navigate to page with no URL")

        return navigateTo(url, constructor)
    }
}
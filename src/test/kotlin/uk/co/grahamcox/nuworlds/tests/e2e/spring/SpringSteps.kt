package uk.co.grahamcox.nuworlds.tests.e2e.spring

import io.cucumber.java8.En
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.annotation.Import
import org.springframework.test.context.ActiveProfiles
import uk.co.grahamcox.nuworlds.tests.integration.IntegrationTestConfig

/**
 * Basis for all of the Spring setup
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@Import(IntegrationTestConfig::class, E2eTestConfig::class)
@ActiveProfiles("test", "e2e")
@AutoConfigureWebMvc
@AutoConfigureMockMvc
class SpringSteps : En {

}
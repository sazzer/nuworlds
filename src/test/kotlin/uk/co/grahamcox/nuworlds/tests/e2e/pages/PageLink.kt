package uk.co.grahamcox.nuworlds.tests.e2e.pages

/**
 * Annotation to indicate the URL for a page model
 */
@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
annotation class PageLink(val url: String)
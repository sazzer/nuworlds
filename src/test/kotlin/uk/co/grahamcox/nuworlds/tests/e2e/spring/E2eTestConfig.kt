package uk.co.grahamcox.nuworlds.tests.e2e.spring

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.testcontainers.containers.Network
import uk.co.grahamcox.nuworlds.authentication.external.google.GoogleSettings
import uk.co.grahamcox.nuworlds.tests.e2e.browser.FakeAuthWrapper
import uk.co.grahamcox.nuworlds.tests.e2e.browser.WebDriverWrapper
import uk.co.grahamcox.nuworlds.tests.e2e.pages.PageLoader
import java.net.URI

/**
 * Spring configuration for the integration tests
 */
@Configuration
class E2eTestConfig {
    /** The container network to use */
    @Autowired
    private lateinit var network: Network

    /** The port the server is listening on */
    @Value("\${server.port}")
    private lateinit var serverPort: Integer

    /**
     * The Embedded WebDriver Container
     */
    @Bean
    fun embeddedWebDriver() = WebDriverWrapper(network, serverPort.toInt())

    /**
     * The Embedded FakeAuth Container
     */
    @Bean(initMethod = "start", destroyMethod = "stop")
    fun embeddedFakeAuth() = FakeAuthWrapper(network)

    /**
     * The actual web driver to use
     */
    @Bean
    fun webdriver(wrapper: WebDriverWrapper) = wrapper.webDriver

    @Bean("googleSettings")
    fun googleSettings(fakeAuth: FakeAuthWrapper): GoogleSettings {
        return GoogleSettings(
                clientId = "googleClientId",
                clientSecret = "googleClientSecret",
                callbackUrl = URI("http://host.testcontainers.internal:${serverPort}/login/google_plus/callback"),
                redirectUrl = URI("http://fakeauth:3000/google/o/oauth2/v2/auth"),
                tokenUrl = URI("http://${fakeAuth.containerIpAddress}:${fakeAuth.containerPort}/google/oauth2/v4/token")
        )
    }

    @Bean
    fun pageLoader(wrapper: WebDriverWrapper) = PageLoader(serverPort, wrapper)
}
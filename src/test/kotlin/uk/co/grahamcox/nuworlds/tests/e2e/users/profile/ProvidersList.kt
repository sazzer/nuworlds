package uk.co.grahamcox.nuworlds.tests.e2e.users.profile

import org.openqa.selenium.By
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.FindBy
import org.openqa.selenium.support.PageFactory
import org.openqa.selenium.support.pagefactory.DefaultElementLocatorFactory

/**
 * Page Model representing the Profile Form
 */
class ProvidersList(base: WebElement) {
    init {
        PageFactory.initElements(DefaultElementLocatorFactory(base), this)
    }

    /** The web elements that represent the provider entries */
    @FindBy(css = ".ui.relaxed.divided.list")
    private lateinit var providerEntries: WebElement

    /** Get the list of providers */
    val providers: List<ProviderEntry>
        get() = providerEntries.findElements(By.className("item"))
                .map(::ProviderEntry)
}
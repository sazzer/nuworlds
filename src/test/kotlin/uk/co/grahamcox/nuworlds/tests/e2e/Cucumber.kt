package uk.co.grahamcox.nuworlds.tests.e2e

import io.cucumber.junit.Cucumber
import io.cucumber.junit.CucumberOptions
import org.junit.jupiter.api.Tag
import org.junit.runner.RunWith

/**
 * Runner for the complete set of E2E Cucumber tests
 */
@RunWith(Cucumber::class)
@Tag("e2e")
@CucumberOptions(
        plugin = ["pretty", "summary", "json:target/e2e/report/cucumber.json"],
        strict = true,
        tags = ["not @wip", "not @ignore", "not @manual"]
)
class AllFeatures

/**
 * Runner for the Work-In-Progress E2E Cucumber tests
 */
@RunWith(Cucumber::class)
@Tag("e2e")
@CucumberOptions(
        plugin = ["pretty", "summary"],
        strict = false,
        tags = ["@wip", "not @ignore", "not @manual"]
)
class WipFeatures
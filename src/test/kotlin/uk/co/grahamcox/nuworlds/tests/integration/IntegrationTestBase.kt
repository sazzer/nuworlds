package uk.co.grahamcox.nuworlds.tests.integration

import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.annotation.Import
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import uk.co.grahamcox.nuworlds.tests.database.DatabaseCleaner
import uk.co.grahamcox.nuworlds.tests.database.SeedData
import uk.co.grahamcox.nuworlds.tests.e2e.database.DatabaseSeeder

/**
 * Base class for the Integration Tests
 */
@ExtendWith(SpringExtension::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@ActiveProfiles("test")
@Import(IntegrationTestConfig::class)
@AutoConfigureWebMvc
@AutoConfigureMockMvc
@Tag("integration")
abstract class IntegrationTestBase {
    /** The means to make MVC requests */
    @Autowired
    protected lateinit var mvc: MockMvc

    /** The means to clean the database before each test */
    @Autowired
    private lateinit var databaseCleaner: DatabaseCleaner

    /** The means to seed the database */
    @Autowired
    private lateinit var databaseSeeder: DatabaseSeeder

    /** The means to seed the database */
    @Autowired
    protected lateinit var jdbcOperations: NamedParameterJdbcOperations

    /**
     * Clean the database before each test
     */
    @BeforeEach
    fun cleanDatabase() {
        databaseCleaner.clean()
    }

    /**
     * Seed the database with some data
     * @param data The data to seed
     * @return The data that was seeded
     */
    fun <T : SeedData> seed(data : T) = databaseSeeder.seed(data)
}
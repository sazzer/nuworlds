package uk.co.grahamcox.nuworlds.tests.e2e.browser

import org.slf4j.LoggerFactory
import org.testcontainers.containers.GenericContainer
import org.testcontainers.containers.Network

/**
 * Wrapper around the FakeAuth Container
 */
class FakeAuthWrapper(network: Network) {
    companion object {
        /** The logger to use */
        private val LOG = LoggerFactory.getLogger(FakeAuthWrapper::class.java)
    }

    /** The postgres server  */
    private val container: GenericContainer<Nothing> = GenericContainer("sazzer/fakeauth")

    init {
        container.withNetwork(network)
        container.addExposedPort(3000)
        container.withNetworkAliases("fakeauth")
    }

    /** The IP Address of the container */
    val containerIpAddress: String
        get() {
            return container.containerIpAddress
        }

    /** The port that is mapped to port 3000 */
    val containerPort: Int
        get() {
            return container.getMappedPort(3000)
        }

    /**
     * Start the server
     */
    fun start() {
        container.start()
        LOG.info("Started FakeAuth Container")
    }

    /**
     * Stop the server
     */
    fun stop() {
        LOG.debug("Stopping FakeAuth Container")
        container.stop()
    }
}
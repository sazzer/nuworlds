package uk.co.grahamcox.nuworlds.tests.e2e.users.profile

import org.openqa.selenium.By
import org.openqa.selenium.NoSuchElementException
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.FindBy
import org.openqa.selenium.support.PageFactory
import org.openqa.selenium.support.pagefactory.DefaultElementLocatorFactory

/**
 * Page Model representing the Profile Form
 */
class ProfileForm(base: WebElement) {
    init {
        PageFactory.initElements(DefaultElementLocatorFactory(base), this)
    }

    /** The web element for the users name */
    @FindBy(name = "name")
    private lateinit var nameElement: WebElement

    /** The web element for the users email address */
    @FindBy(name = "email")
    private lateinit var emailElement: WebElement

    /** The web element for the Save button */
    @FindBy(css = ".ui.primary.button")
    private lateinit var saveButtonElement: WebElement

    /** The web element for the Errors */
    @FindBy(css = ".ui.error.message")
    private lateinit var errorsElement: WebElement

    /** The web element for the Success message */
    @FindBy(css = ".ui.positive.message")
    private lateinit var successElement: WebElement

    /** The users name */
    var name: String
        get() = nameElement.getAttribute("value")
        set(value: String) {
            nameElement.clear()
            nameElement.click()
            nameElement.sendKeys(value)
        }

    /** The users email address */
    var email: String
        get() = emailElement.getAttribute("value")
        set(value: String) {
            emailElement.clear()
            emailElement.click()
            emailElement.sendKeys(value)
        }

    /**
     * Get the list of errors for the form
     */
    val errors: List<String>
        get() {
            return errorsElement.findElements(By.tagName("li"))
                    .map { it.text }
        }

    /**
     * Get the success message, if it's visible
     */
    val successMessage: String?
        get() {
            return try {
                if (successElement.isDisplayed) {
                    successElement.text
                } else {
                    null
                }
            } catch (e: NoSuchElementException) {
                null
            }
        }
    /**
     * Save the form
     */
    fun save() {
        saveButtonElement.click()
    }
}
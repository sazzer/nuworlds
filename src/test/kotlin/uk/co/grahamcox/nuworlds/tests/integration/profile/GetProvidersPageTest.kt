package uk.co.grahamcox.nuworlds.tests.integration.profile

import com.karumi.kotlinsnapshot.matchWithSnapshot
import org.jsoup.Jsoup
import org.junit.jupiter.api.Test
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

/**
 * Integration Tests for rendering the Profile Page
 */
class GetProvidersPageTest : ProfilePageTestBase() {
    @Test
    fun getProvidersPageUnauthenticated() {
        val response = mvc.perform(MockMvcRequestBuilders.get("/profile/providers"))
                .andExpect(MockMvcResultMatchers.status().isForbidden)
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith("text/html"))
                .andExpect(MockMvcResultMatchers.view().name("unauthenticatedError"))
                .andReturn()
                .response

        Jsoup.parse(response.contentAsString).normalise().html().matchWithSnapshot()
    }

    @Test
    fun getProvidersPageAuthenticated() {
        val response = mvc.perform(MockMvcRequestBuilders.get("/profile/providers")
                .cookie(authCookie))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith("text/html"))
                .andExpect(MockMvcResultMatchers.view().name("profile/providers"))
                .andReturn()
                .response

        Jsoup.parse(response.contentAsString).normalise().html().matchWithSnapshot()
    }
}
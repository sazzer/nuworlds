package uk.co.grahamcox.nuworlds.tests.integration.profile

import ch.tutteli.atrium.api.cc.en_GB.returnValueOf
import ch.tutteli.atrium.api.cc.en_GB.toBe
import ch.tutteli.atrium.verbs.expect
import com.karumi.kotlinsnapshot.matchWithSnapshot
import org.jsoup.Jsoup
import org.junit.jupiter.api.Test
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

/**
 * Integration Tests for rendering the Profile Page
 */
class GetProfilePageTest : ProfilePageTestBase() {
    @Test
    fun getProfilePageUnauthenticated() {
        val response = mvc.perform(MockMvcRequestBuilders.get("/profile"))
                .andExpect(MockMvcResultMatchers.status().isForbidden)
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith("text/html"))
                .andExpect(MockMvcResultMatchers.view().name("unauthenticatedError"))
                .andReturn()
                .response

        Jsoup.parse(response.contentAsString).normalise().html().matchWithSnapshot()
    }

    @Test
    fun getProfilePageAuthenticated() {
        val response = mvc.perform(MockMvcRequestBuilders.get("/profile")
                .cookie(authCookie))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith("text/html"))
                .andExpect(MockMvcResultMatchers.view().name("profile/main"))
                .andReturn()
                .response

        Jsoup.parse(response.contentAsString).normalise().html().matchWithSnapshot()
    }

    @Test
    fun getProfilePageAfterSuccess() {
        val response = mvc.perform(MockMvcRequestBuilders.get("/profile")
                .param("success", "")
                .cookie(authCookie))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith("text/html"))
                .andExpect(MockMvcResultMatchers.view().name("profile/main"))
                .andReturn()
                .response

        val parsed = Jsoup.parse(response.contentAsString)
        expect(parsed.select("""div.ui.positive.message""")) {
            returnValueOf(subject::text).toBe("User profile saved successfully")
        }
        parsed.normalise().html().matchWithSnapshot()
    }
}
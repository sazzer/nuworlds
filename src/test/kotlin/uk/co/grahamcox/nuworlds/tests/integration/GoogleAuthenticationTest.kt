package uk.co.grahamcox.nuworlds.tests.integration

import ch.tutteli.atrium.api.cc.en_GB.contains
import ch.tutteli.atrium.api.cc.en_GB.containsKey
import ch.tutteli.atrium.api.cc.en_GB.returnValueOf
import ch.tutteli.atrium.api.cc.en_GB.toBe
import ch.tutteli.atrium.verbs.expect
import org.junit.jupiter.api.Test
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.web.util.UriComponentsBuilder

/**
 * Integration tests for authentication via Google
 */
class GoogleAuthenticationTest : IntegrationTestBase() {
    /**
     * Test redirecting to Google to authenticate
     */
    @Test
    fun redirectToStart() {
        val response = mvc.perform(MockMvcRequestBuilders.post("/login/google_plus/start"))
                .andExpect(MockMvcResultMatchers.status().isFound)
                .andExpect(MockMvcResultMatchers.header().exists("Location"))
                .andReturn()
                .response

        val locationHeader = response.getHeader("Location")!!

        val uriComponents = UriComponentsBuilder.fromUriString(locationHeader).build()

        expect(uriComponents) {
            returnValueOf(subject::getScheme).toBe("http")
            returnValueOf(subject::getHost).toBe("fakeauth")
            returnValueOf(subject::getPort).toBe(3000)
            returnValueOf(subject::getPath).toBe("/google/o/oauth2/v2/auth")

            returnValueOf(subject::getQueryParams).contains("client_id" to listOf("googleClientId"))
            // Note - the port is "-1" because of SpringBootTest.WebEnvironment.MOCK
            returnValueOf(subject::getQueryParams).contains("redirect_uri" to listOf("http://host.testcontainers.internal:-1/login/google_plus/callback"))
            returnValueOf(subject::getQueryParams).contains("response_type" to listOf("code"))
            returnValueOf(subject::getQueryParams).contains("scope" to listOf("openid%20email%20profile"))
            returnValueOf(subject::getQueryParams).containsKey("state")
        }

    }

}
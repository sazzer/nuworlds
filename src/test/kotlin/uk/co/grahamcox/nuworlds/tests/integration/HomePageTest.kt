package uk.co.grahamcox.nuworlds.tests.integration

import com.karumi.kotlinsnapshot.matchWithSnapshot
import org.jsoup.Jsoup
import org.junit.jupiter.api.Test
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*

/**
 * Test the rendering of the home page
 */
class HomePageTest : IntegrationTestBase() {

    /**
     * Test rendering the home page
     */
    @Test
    fun loadHomePage() {
        val response = mvc.perform(MockMvcRequestBuilders.get("/"))
                .andExpect(status().isOk)
                .andExpect(content().contentTypeCompatibleWith("text/html"))
                .andExpect(view().name("home"))
                .andReturn()
                .response

        Jsoup.parse(response.contentAsString).normalise().html().matchWithSnapshot()
    }

}

package uk.co.grahamcox.nuworlds.tests.e2e.pages

import org.openqa.selenium.SearchContext
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.FindBy
import org.openqa.selenium.support.PageFactory
import org.openqa.selenium.support.pagefactory.DefaultElementLocatorFactory
import uk.co.grahamcox.nuworlds.tests.e2e.home.ProfileMenu

/**
 * Base class from which all others derive
 */
open class BasePage(webdriver: SearchContext) {
    init {
        PageFactory.initElements(DefaultElementLocatorFactory(webdriver), this)
    }

    /** The web element wrapping the profile menu */
    @FindBy(css = """[data-test="profileMenu"]""")
    private lateinit var profileMenuElement: WebElement

    /** Get the profile menu */
    val profileMenu
        get() = ProfileMenu(profileMenuElement)
}
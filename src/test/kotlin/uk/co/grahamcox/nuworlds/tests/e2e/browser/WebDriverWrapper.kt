package uk.co.grahamcox.nuworlds.tests.e2e.browser

import org.openqa.selenium.remote.DesiredCapabilities
import org.openqa.selenium.remote.RemoteWebDriver
import org.slf4j.LoggerFactory
import org.testcontainers.Testcontainers
import org.testcontainers.containers.BrowserWebDriverContainer
import org.testcontainers.containers.Network
import org.testcontainers.lifecycle.TestDescription
import java.io.File
import java.util.*

/**
 * Wrapper around the WebDriver Container
 */
class WebDriverWrapper(network: Network, serverPort: Int) {
    companion object {
        /** The logger to use */
        private val LOG = LoggerFactory.getLogger(WebDriverWrapper::class.java)
    }

    /** The postgres server  */
    private val container: BrowserWebDriverContainer<Nothing> = BrowserWebDriverContainer()

    init {
        val videosDirectory = File("./target/e2e/videos")
        videosDirectory.mkdirs()

        container.withNetwork(network)
        container.withCapabilities(DesiredCapabilities.chrome())
        container.withRecordingMode(BrowserWebDriverContainer.VncRecordingMode.RECORD_ALL, videosDirectory)
        Testcontainers.exposeHostPorts(serverPort)
    }

    /**
     * Start the server
     */
    fun start() {
        container.start()
        LOG.info("Started WebDriver Container")
    }

    /**
     * Stop the server
     */
    fun stop() {
        LOG.debug("Stopping WebDriver Container")
        container.stop()
    }

    /**
     * Save the recorded video of the test
     */
    fun record(testMethod: String) {
        container.afterTest(object : TestDescription {
            override fun getFilesystemFriendlyName() = testMethod.replace("[^A-Za-z0-9]".toRegex(), "_")

            override fun getTestId() = testMethod
        }, Optional.empty())
    }
    /**
     * Get the Web Driver to use
     */
    val webDriver: RemoteWebDriver
        get() = container.webDriver
}
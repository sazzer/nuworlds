package uk.co.grahamcox.nuworlds.tests.e2e.home

import org.openqa.selenium.WebElement
import org.openqa.selenium.support.FindBy
import org.openqa.selenium.support.PageFactory
import org.openqa.selenium.support.pagefactory.DefaultElementLocatorFactory

/**
 * Page Object representing the Profile Menu
 */
class ProfileMenu(base: WebElement) {
    init {
        PageFactory.initElements(DefaultElementLocatorFactory(base), this)
    }

    /** The web element containing the current user name */
    @FindBy(className = "text")
    private lateinit var userNameElement: WebElement

    /** The web element representing the link to the Profile page */
    @FindBy(css = """[data-test="profile"]""")
    private lateinit var profileMenuElement: WebElement

    /** Get the current users name */
    val userName: String
        get() = userNameElement.text

    /** Navigate to the profile page */
    fun visitProfilePage() {
        if (!profileMenuElement.isDisplayed) {
            userNameElement.click()
        }
        profileMenuElement.click()
    }
}
package uk.co.grahamcox.nuworlds.tests.e2e.users

import ch.tutteli.atrium.api.cc.en_GB.*
import ch.tutteli.atrium.verbs.expect
import io.cucumber.datatable.DataTable
import io.cucumber.java8.En
import org.springframework.beans.factory.annotation.Autowired
import uk.co.grahamcox.nuworlds.tests.e2e.pages.BasePage
import uk.co.grahamcox.nuworlds.tests.e2e.pages.PageLoader
import uk.co.grahamcox.nuworlds.tests.e2e.users.profile.ProfilePage

/**
 * Cucumber Steps for working with the User Profile Page
 */
class UserProfilePageSteps : En {
    /** The page loader to use */
    @Autowired
    private lateinit var pageLoader: PageLoader

    init {
        When("^I view the user profile page$") {
            val basePage = pageLoader.getPageModel(::BasePage)
            basePage.profileMenu.visitProfilePage()
        }

        When("^I view the authentication providers list on the user profile page$") {
            val profilePage = pageLoader.getPageModel(::ProfilePage)
            profilePage.openProvidersList()
        }

        When("^I update the user profile with details:$") { details: DataTable ->
            val profilePage = pageLoader.getPageModel(::ProfilePage)
            val profileForm = profilePage.profileForm

            details.asMap<String, String>(String::class.java, String::class.java)
                    .toList()
                    .forEach { (key, value) ->
                        when (key) {
                            "Name" -> profileForm.name = value
                            "Email Address" -> profileForm.email = value
                            else -> throw IllegalArgumentException("Unexpected field for user profile form: $key")
                        }
                    }

            profileForm.save()
        }

        Then("^the user profile is saved successfully$") {
            val profilePage = pageLoader.getPageModel(::ProfilePage)
            val profileForm = profilePage.profileForm
            expect(profileForm) {
                property(subject::successMessage).toBe("User profile saved successfully")
                property(subject::errors).hasSize(0)
            }

        }

        Then("^the user profile saved with errors:$") { errors: DataTable ->
            val profilePage = pageLoader.getPageModel(::ProfilePage)
            val profileForm = profilePage.profileForm
            val expectedErrors = errors.asList()
            expect(profileForm) {
                property(subject::successMessage).toBe(null)
                property(subject::errors).containsExactly(expectedErrors.first(), *expectedErrors.drop(1).toTypedArray())
            }

        }

        Then("^the user profile has values:$") { profile: DataTable ->
            val profilePage = pageLoader.getPageModel(::ProfilePage)
            val profileForm = profilePage.profileForm

            expect(profileForm) {
                profile.asMap<String, String>(String::class.java, String::class.java)
                        .toList()
                        .forEach { (key, value) ->
                            when (key) {
                                "Name" -> property(subject::name).toBe(value)
                                "Email Address" -> property(subject::email).toBe(value)
                                else -> throw IllegalArgumentException("Unexpected field for user profile form: $key")
                            }
                        }
            }
        }

        Then("^the user profile has authentication provider:$") { provider: DataTable ->
            val profilePage = pageLoader.getPageModel(::ProfilePage)
            val providersPage = profilePage.providersList
            expect(providersPage.providers).contains {
                provider.asMap<String, String>(String::class.java, String::class.java)
                        .toList()
                        .forEach { (key, value) ->
                            when (key) {
                                "Provider" -> property(subject::providerName).toBe(value)
                                "Display Name" -> property(subject::displayName).toBe(value)
                                else -> throw IllegalArgumentException("Unexpected field for user providers list: $key")
                            }
                        }
            }
        }
    }
}
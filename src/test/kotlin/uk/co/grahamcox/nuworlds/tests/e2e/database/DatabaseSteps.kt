package uk.co.grahamcox.nuworlds.tests.e2e.database

import io.cucumber.java8.En
import org.springframework.beans.factory.annotation.Autowired
import uk.co.grahamcox.nuworlds.tests.database.DatabaseCleaner

/**
 * Cucumber setup for ensuring the database is in the correct state
 */
class DatabaseSteps : En {
    /** The means to clean the database before each test */
    @Autowired
    private lateinit var databaseCleaner: DatabaseCleaner

    init {
        Before { scenario ->
            databaseCleaner.clean()
        }
    }
}
package uk.co.grahamcox.nuworlds.tests.integration.profile

import org.junit.jupiter.api.BeforeEach
import org.springframework.beans.factory.annotation.Autowired
import uk.co.grahamcox.nuworlds.authentication.token.AccessToken
import uk.co.grahamcox.nuworlds.authentication.token.AccessTokenSerializer
import uk.co.grahamcox.nuworlds.tests.integration.IntegrationTestBase
import uk.co.grahamcox.nuworlds.users.AuthenticationDetails
import uk.co.grahamcox.nuworlds.users.UserId
import uk.co.grahamcox.nuworlds.users.dao.UserSeed
import java.util.*
import javax.servlet.http.Cookie

/**
 * Base class for the Profile Page tests
 */
open class ProfilePageTestBase : IntegrationTestBase() {
    /** The user to work with */
    private lateinit var user: UserSeed

    /** The cookie to use for authentication */
    protected lateinit var authCookie: Cookie

    /** The means to serialize an access token */
    @Autowired
    private lateinit var accessTokenSerializer: AccessTokenSerializer

    /**
     * Seed the user to work with
     */
    @BeforeEach
    fun seedUser() {
        user = seed(UserSeed(
                userId = UserId(UUID.fromString("6f5f9a96-1ead-4a6f-9986-0a18c5ab9bce")),
                name = "Test User",
                email = "testuser@example.com",
                authentication = setOf(
                        AuthenticationDetails("google_plus", "googlePlusId", "testuser@example.com"),
                        AuthenticationDetails("twitter", "twitterId", "@testuser"),
                        AuthenticationDetails("facebook", "facebookId", "Test User")
                )
        ))

        authCookie = Cookie("nuworlds_access_token", accessTokenSerializer.serialize(AccessToken(
                tokenId = UUID.randomUUID(),
                userId = user.userId
        )))
    }

}
package uk.co.grahamcox.nuworlds.tests.e2e.database

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations
import uk.co.grahamcox.nuworlds.tests.database.SeedData

/**
 * Mechanism by which we can seed the database
 */
class DatabaseSeeder {
    /** The means to update the database */
    @Autowired
    private lateinit var jdbcOperations: NamedParameterJdbcOperations

    /**
     * Seed the database with some data
     * @param data The data to seed
     * @return The data that was seeded
     */
    fun <T : SeedData> seed(data : T) : T {
        jdbcOperations.update(data.sql, data.binds)
        return data
    }
}
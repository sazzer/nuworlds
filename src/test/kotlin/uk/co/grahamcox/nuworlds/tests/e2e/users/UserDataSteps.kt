package uk.co.grahamcox.nuworlds.tests.e2e.users

import ch.tutteli.atrium.api.cc.en_GB.contains
import ch.tutteli.atrium.verbs.expect
import io.cucumber.datatable.DataTable
import io.cucumber.java8.En
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations
import uk.co.grahamcox.nuworlds.tests.e2e.database.DatabaseSeeder
import uk.co.grahamcox.nuworlds.users.AuthenticationDetails
import uk.co.grahamcox.nuworlds.users.UserId
import uk.co.grahamcox.nuworlds.users.dao.UserSeed
import java.util.*

/**
 * Steps for seeding user details
 */
class UserDataSteps : En {
    /** The means to seed the database */
    @Autowired
    private lateinit var databaseSeeder: DatabaseSeeder

    /** The means to access the database */
    @Autowired
    private lateinit var jdbcOperations: NamedParameterJdbcOperations

    init {
        Given("^a user exists with details:$") { dataTable: DataTable ->
            val userSeed = dataTable.asMap<String, String>(String::class.java, String::class.java)
                    .toList()
                    .fold(UserSeed()) { acc: UserSeed, next: Pair<String, String> ->
                        when (next.first) {
                            "User ID" -> acc.copy(userId = UserId(UUID.fromString(next.second)))
                            "Name" -> acc.copy(name = next.second)
                            "Email Address" -> acc.copy(email = next.second)
                            "Authentication Details" -> {
                                val authenticationDetails = next.second.split(",")
                                        .map { auth ->
                                            val (provider, providerId, displayName) = auth.split(";")
                                            AuthenticationDetails(provider, providerId, displayName)
                                        }
                                        .toSet()
                                acc.copy(authentication = authenticationDetails)
                            }
                            else -> throw IllegalArgumentException("Unexpected field for seeding user: $next")
                        }
                    }
            databaseSeeder.seed(userSeed)
        }

        Then("^there is a user in the database matching:$") { dataTable: DataTable ->
            val assertions = dataTable.asMap<String, String>(String::class.java, String::class.java)
                    .toList()
                    .map { (key, value) ->
                        when (key) {
                            "User ID" -> "user_id" to UUID.fromString(value)
                            "Name" -> "name" to value
                            "Email Address" -> "email" to value
                            else -> throw IllegalArgumentException("Unexpected field for matching user: $key")
                        }
                    }

            val usersList = jdbcOperations.queryForList("SELECT * FROM users", emptyMap<String, Any?>())

            expect(usersList).contains { contains(assertions.first(), *assertions.drop(1).toTypedArray()) }
        }
    }
}
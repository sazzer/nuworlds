package uk.co.grahamcox.nuworlds.tests.e2e.home

import ch.tutteli.atrium.api.cc.en_GB.property
import ch.tutteli.atrium.api.cc.en_GB.returnValueOf
import ch.tutteli.atrium.api.cc.en_GB.toBe
import ch.tutteli.atrium.verbs.expect
import io.cucumber.datatable.DataTable
import io.cucumber.java8.En
import org.springframework.beans.factory.annotation.Autowired
import uk.co.grahamcox.nuworlds.tests.e2e.pages.BasePage
import uk.co.grahamcox.nuworlds.tests.e2e.pages.PageLoader

/**
 * Cucumber Steps for working with the home page
 */
class HomePageSteps : En {
    /** The page loader to use */
    @Autowired
    private lateinit var pageLoader: PageLoader

    init {
        Given("^I load the home page$") {
            pageLoader.navigateTo(::HomePage)
        }

        When("^I authenticate using \"(.+)\"$") { provider: String ->
            val homePage = pageLoader.getPageModel(::HomePage)
            homePage.authenticationArea.authenticate(provider)
        }

        Then("^I can authenticate using:$") { providers: DataTable ->
            val page = pageLoader.getPageModel(::HomePage)
            expect(page.authenticationArea) {
                providers.asList().forEach { provider ->
                    returnValueOf(subject::canAuthenticate, provider).toBe(true)
                }
            }
        }

        Then("^I am logged in as \"(.+)\"$") { user: String ->
            val page = pageLoader.getPageModel(::BasePage)
            expect(page.profileMenu) {
                property(subject::userName).toBe(user)
            }
        }
    }
}
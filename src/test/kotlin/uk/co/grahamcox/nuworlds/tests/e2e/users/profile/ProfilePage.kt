package uk.co.grahamcox.nuworlds.tests.e2e.users.profile

import org.openqa.selenium.SearchContext
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.FindBy
import uk.co.grahamcox.nuworlds.tests.e2e.pages.BasePage
import uk.co.grahamcox.nuworlds.tests.e2e.pages.PageLink

/**
 * Page Model for the user profile page
 */
@PageLink("/profile")
class ProfilePage(webdriver: SearchContext) : BasePage(webdriver) {
    /** Web element for the menu entry for the profile form */
    @FindBy(css = """.four.wide.column [data-test="profileMenuEntry"]""")
    private lateinit var profileFormMenuEntry: WebElement

    /** Web element for the menu entry for the providers list */
    @FindBy(css = """.four.wide.column [data-test="providersMenuEntry"]""")
    private lateinit var providersListMenuEntry: WebElement

    /** Web element for the main area */
    @FindBy(css = ".twelve.wide.column")
    private lateinit var mainArea: WebElement

    /** Get the profile form */
    val profileForm: ProfileForm
        get() {
            return ProfileForm(mainArea)
        }

    /** Get the providers list */
    val providersList: ProvidersList
        get() {
            return ProvidersList(mainArea)
        }

    /**
     * Open the Profile form
     */
    fun openProfileForm() {
        profileFormMenuEntry.click()
    }

    /**
     * Open the Providers list
     */
    fun openProvidersList() {
        providersListMenuEntry.click()
    }
}
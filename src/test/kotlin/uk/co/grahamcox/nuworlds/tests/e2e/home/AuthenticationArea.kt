package uk.co.grahamcox.nuworlds.tests.e2e.home

import org.openqa.selenium.WebElement
import org.openqa.selenium.support.FindBy
import org.openqa.selenium.support.PageFactory
import org.openqa.selenium.support.pagefactory.DefaultElementLocatorFactory

/**
 * Page Object to represent the Authentication Area
 */
class AuthenticationArea(base: WebElement) {
    init {
        PageFactory.initElements(DefaultElementLocatorFactory(base), this)
    }

    /** The button for triggering Google Auth */
    @FindBy(css = """button[data-test="authentication-google_plus"]""")
    private lateinit var googleButton: WebElement

    /** The map of buttons to work with */
    private val buttons: Map<String, WebElement>
        get() = mapOf(
                "google_plus" to googleButton
        )

    /**
     * Check if we can authenticate with the given provider
     * @param with The authentication provider to use
     * @return Whether the provider is an option
     */
    fun canAuthenticate(with: String) : Boolean {
        return buttons[with]?.isDisplayed ?: false
    }

    /**
     * Start authentication with the given provider
     * @param with The authentication provider to use
     */
    fun authenticate(with: String) {
        val button = buttons[with] ?: throw IllegalArgumentException("Unsupported authentication provider: $with")
        button.click()
    }
}
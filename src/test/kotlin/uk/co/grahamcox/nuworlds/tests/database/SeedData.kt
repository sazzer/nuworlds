package uk.co.grahamcox.nuworlds.tests.database

/**
 * Interface describing some piece of seed data to work with
 */
interface SeedData {
    /** The SQL to insert the data with */
    val sql: String

    /** The binds for the seed data */
    val binds: Map<String, Any?>
}
package uk.co.grahamcox.nuworlds.tests.e2e.home

import org.openqa.selenium.SearchContext
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.FindBy
import uk.co.grahamcox.nuworlds.tests.e2e.pages.BasePage
import uk.co.grahamcox.nuworlds.tests.e2e.pages.PageLink

/**
 * Page Object to represent the Home Page
 */
@PageLink("")
class HomePage(webdriver: SearchContext) : BasePage(webdriver) {
    /** The web element wrapping the authentication area */
    @FindBy(css = """[data-test="authentication"]""")
    private lateinit var authenticationAreaElement: WebElement

    /** The authentication area */
    val authenticationArea: AuthenticationArea
        get() = AuthenticationArea(authenticationAreaElement)
}
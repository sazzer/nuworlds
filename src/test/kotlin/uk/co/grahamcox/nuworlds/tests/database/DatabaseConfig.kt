package uk.co.grahamcox.nuworlds.tests.database

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.jdbc.DataSourceBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.testcontainers.containers.Network

/**
 * Spring configuration for the database
 */
@Configuration
class DatabaseConfig {
    /** The container network to use */
    @Autowired
    private lateinit var network: Network

    /**
     * The Embedded Postgres server
     */
    @Bean(initMethod = "start", destroyMethod = "stop")
    fun embeddedPostgres() = PostgresWrapper(network)

    /**
     * The data source to use
     */
    @Bean
    fun datasource(postgres: PostgresWrapper) = DataSourceBuilder.create()
            .url(postgres.url)
            .username("worlds")
            .password("worlds")
            .build()
}
package uk.co.grahamcox.nuworlds.users.dao

import ch.tutteli.atrium.api.cc.en_GB.*
import ch.tutteli.atrium.verbs.expect
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import uk.co.grahamcox.nuworlds.model.Identity
import uk.co.grahamcox.nuworlds.tests.integration.IntegrationTestBase
import uk.co.grahamcox.nuworlds.users.*
import java.util.*

/**
 * Tests for the User DAO
 */
internal class UserJdbcDaoTest : IntegrationTestBase() {
    /** The test subject */
    @Autowired
    private lateinit var testSubject: UserJdbcDao

    /**
     * Test getting a user by ID when the user doesn't exist
     */
    @Test
    fun getUnknownUserById() {
        val userId = UserId(UUID.randomUUID())
        expect {
            testSubject.getById(userId)
        }.toThrow<UnknownUserException> {
            property(subject::id) { toBe(userId) }
        }
    }

    /**
     * Test getting a user by ID when the user doesn't exist
     */
    @Test
    fun getKnownUserById() {
        val seed = seed(UserSeed())

        val user = testSubject.getById(seed.userId)

        expect(user) {
            property(subject::identity).property(Identity<UserId>::id).toBe(seed.userId)
            property(subject::identity).property(Identity<UserId>::version).toBe(seed.version)
            property(subject::identity).property(Identity<UserId>::created).toBe(seed.created)
            property(subject::identity).property(Identity<UserId>::updated).toBe(seed.updated)
            property(subject::data).property(UserData::name).toBe(seed.name)
            property(subject::data).property(UserData::email).toBe(seed.email)
            property(subject::data).property(UserData::authenticationDetails).isEmpty()
        }
    }

    /**
     * Test getting a user by ID when the user doesn't exist
     */
    @Test
    fun getKnownUserById_AuthDetails() {
        val seed = seed(UserSeed(
                authentication = setOf(
                        AuthenticationDetails(
                                provider = "twitter",
                                providerId = "@12345678",
                                displayName = "@12345678"
                        ),
                        AuthenticationDetails(
                                provider = "google",
                                providerId = "123456",
                                displayName = "test@user.com"
                        )
                )
        ))

        val user = testSubject.getById(seed.userId)

        expect(user) {
            property(subject::identity).property(Identity<UserId>::id).toBe(seed.userId)
            property(subject::identity).property(Identity<UserId>::version).toBe(seed.version)
            property(subject::identity).property(Identity<UserId>::created).toBe(seed.created)
            property(subject::identity).property(Identity<UserId>::updated).toBe(seed.updated)
            property(subject::data).property(UserData::name).toBe(seed.name)
            property(subject::data).property(UserData::email).toBe(seed.email)
            property(subject::data).property(UserData::authenticationDetails).contains.inOrder.only.values(
                    AuthenticationDetails(
                            provider = "google",
                            providerId = "123456",
                            displayName = "test@user.com"
                    ),
                    AuthenticationDetails(
                            provider = "twitter",
                            providerId = "@12345678",
                            displayName = "@12345678"
                    )
            )
        }
    }

    @Test
    fun findUnknownUserByProviderId() {
        val user = testSubject.findUserByProviderId("google", "123456")
        expect(user).toBe(null)
    }

    @Test
    fun findUserByMismatchedProviderId() {
        val seed = seed(UserSeed(
                authentication = setOf(
                        AuthenticationDetails(
                                provider = "twitter",
                                providerId = "@12345678",
                                displayName = "@12345678"
                        ),
                        AuthenticationDetails(
                                provider = "google",
                                providerId = "123456",
                                displayName = "test@user.com"
                        )
                )
        ))

        val user = testSubject.findUserByProviderId("twitter", "123456")
        expect(user).toBe(null)
    }

    @Test
    fun findKnownUserByProviderId() {
        val seed = seed(UserSeed(
                authentication = setOf(
                        AuthenticationDetails(
                                provider = "twitter",
                                providerId = "@12345678",
                                displayName = "@12345678"
                        ),
                        AuthenticationDetails(
                                provider = "google",
                                providerId = "123456",
                                displayName = "test@user.com"
                        )
                )
        ))

        val user = testSubject.findUserByProviderId("google", "123456")

        expect(user).notToBeNull {
            property(subject::identity).property(Identity<UserId>::id).toBe(seed.userId)
            property(subject::identity).property(Identity<UserId>::version).toBe(seed.version)
            property(subject::identity).property(Identity<UserId>::created).toBe(seed.created)
            property(subject::identity).property(Identity<UserId>::updated).toBe(seed.updated)
            property(subject::data).property(UserData::name).toBe(seed.name)
            property(subject::data).property(UserData::email).toBe(seed.email)
            property(subject::data).property(UserData::authenticationDetails).contains.inOrder.only.values(
                    AuthenticationDetails(
                            provider = "google",
                            providerId = "123456",
                            displayName = "test@user.com"
                    ),
                    AuthenticationDetails(
                            provider = "twitter",
                            providerId = "@12345678",
                            displayName = "@12345678"
                    )
            )
        }
    }

    @Test
    fun createUser() {
        val user = testSubject.create(UserData(
                name = "Test User",
                email = "test@user.com",
                authenticationDetails = listOf(
                        AuthenticationDetails(
                                provider = "twitter",
                                providerId = "@12345678",
                                displayName = "@12345678"
                        ),
                        AuthenticationDetails(
                                provider = "google",
                                providerId = "123456",
                                displayName = "test@user.com"
                        )
                )
        ))

        expect(user) {
            property(subject::data).property(UserData::name).toBe("Test User")
            property(subject::data).property(UserData::email).toBe("test@user.com")
            property(subject::data).property(UserData::authenticationDetails).contains.inOrder.only.values(
                    AuthenticationDetails(
                            provider = "google",
                            providerId = "123456",
                            displayName = "test@user.com"
                    ),
                    AuthenticationDetails(
                            provider = "twitter",
                            providerId = "@12345678",
                            displayName = "@12345678"
                    )
            )
        }
    }

    @Test
    fun updateUser() {
        val seed = seed(UserSeed(
                name = "Old Name",
                email = "old@example.com",
                authentication = setOf(
                        AuthenticationDetails(
                                provider = "google",
                                providerId = "123456",
                                displayName = "test@user.com"
                        )
                )
        ))

        val updated = testSubject.updateUser(seed.userId) { user ->
            user.copy(
                    name = "New Name",
                    email = "new@example.com",
                    authenticationDetails = setOf(
                            AuthenticationDetails(
                                    provider = "twitter",
                                    providerId = "@12345678",
                                    displayName = "@12345678"
                            ),
                            AuthenticationDetails(
                                    provider = "google",
                                    providerId = "123456",
                                    displayName = "test@user.com"
                            )
                    )
            )
        }

        expect(updated) {
            property(subject::identity).property(Identity<UserId>::id).toBe(seed.userId)
            property(subject::identity).property(Identity<UserId>::version).notToBe(seed.version)
            property(subject::identity).property(Identity<UserId>::created).toBe(seed.created)
            property(subject::identity).property(Identity<UserId>::updated).notToBe(seed.updated)
            property(subject::data).property(UserData::name).toBe("New Name")
            property(subject::data).property(UserData::email).toBe("new@example.com")
            property(subject::data).property(UserData::authenticationDetails).contains.inOrder.only.values(
                    AuthenticationDetails(
                            provider = "google",
                            providerId = "123456",
                            displayName = "test@user.com"
                    ),
                    AuthenticationDetails(
                            provider = "twitter",
                            providerId = "@12345678",
                            displayName = "@12345678"
                    )
            )
        }
    }

    @Test
    fun updateRefetchUser() {
        val seed = seed(UserSeed(
                name = "Old Name",
                email = "old@example.com",
                authentication = setOf(
                        AuthenticationDetails(
                                provider = "google",
                                providerId = "123456",
                                displayName = "test@user.com"
                        )
                )
        ))

        testSubject.updateUser(seed.userId) { user ->
            user.copy(
                    name = "New Name",
                    email = "new@example.com",
                    authenticationDetails = setOf(
                            AuthenticationDetails(
                                    provider = "twitter",
                                    providerId = "@12345678",
                                    displayName = "@12345678"
                            ),
                            AuthenticationDetails(
                                    provider = "google",
                                    providerId = "123456",
                                    displayName = "test@user.com"
                            )
                    )
            )
        }

        val updated = testSubject.getById(seed.userId)
        expect(updated) {
            property(subject::identity).property(Identity<UserId>::id).toBe(seed.userId)
            property(subject::identity).property(Identity<UserId>::version).notToBe(seed.version)
            property(subject::identity).property(Identity<UserId>::created).toBe(seed.created)
            property(subject::identity).property(Identity<UserId>::updated).notToBe(seed.updated)
            property(subject::data).property(UserData::name).toBe("New Name")
            property(subject::data).property(UserData::email).toBe("new@example.com")
            property(subject::data).property(UserData::authenticationDetails).contains.inOrder.only.values(
                    AuthenticationDetails(
                            provider = "google",
                            providerId = "123456",
                            displayName = "test@user.com"
                    ),
                    AuthenticationDetails(
                            provider = "twitter",
                            providerId = "@12345678",
                            displayName = "@12345678"
                    )
            )
        }
    }

    @Test
    fun updateUnknownUser() {
        val userId = UserId(UUID.randomUUID())
        expect {
            testSubject.updateUser(userId) { user -> user }
        }.toThrow<UnknownUserException> {
            property(subject::id).toBe(userId)
        }
    }
}
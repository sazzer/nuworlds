package uk.co.grahamcox.nuworlds.users.dao

import com.fasterxml.jackson.databind.ObjectMapper
import uk.co.grahamcox.nuworlds.tests.database.SeedData
import uk.co.grahamcox.nuworlds.users.AuthenticationDetails
import uk.co.grahamcox.nuworlds.users.UserId
import java.time.Instant
import java.util.*

/**
 * Seed Data for seeding a user
 */
data class UserSeed(
        val userId : UserId = UserId(UUID.randomUUID()),
        val version: UUID = UUID.randomUUID(),
        val created: Instant = Instant.parse("2019-09-23T08:12:00Z"),
        val updated: Instant = Instant.parse("2019-09-23T08:13:00Z"),
        val name: String = "Test User",
        val email: String = "testuser@example.com",
        val authentication: Set<AuthenticationDetails> = emptySet()
) : SeedData {
    override val sql = """
        INSERT INTO users(user_id, version, created, updated, name, email, authentication)
        VALUES (:userId, :version, :created, :updated, :name, :email, :authentication::jsonb)
        """

    override val binds = mapOf(
            "userId" to userId.id,
            "version" to version,
            "created" to Date.from(created),
            "updated" to Date.from(updated),
            "name" to name,
            "email" to email,
            "authentication" to ObjectMapper().writeValueAsString(this.authentication)
    )
}
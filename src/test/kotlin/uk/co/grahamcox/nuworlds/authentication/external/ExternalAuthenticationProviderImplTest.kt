package uk.co.grahamcox.nuworlds.authentication.external

import ch.tutteli.atrium.api.cc.en_GB.toBe
import ch.tutteli.atrium.verbs.expect
import io.mockk.confirmVerified
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import java.net.URI
import java.util.*

/**
 * Unit tests for the External Authentication Provider
 */
internal class ExternalAuthenticationProviderImplTest {
    /** The Start Authentication Strategy */
    private val startAuthenticationStrategy = mockk<StartAuthenticationStrategy>()

    /** The Complete Authentication Strategy */
    private val completeAuthenticationStrategy = mockk<CompleteAuthenticationStrategy>()

    /** The test subject */
    private val testSubject = ExternalAuthenticationProviderImpl("testing", true,
            startAuthenticationStrategy, completeAuthenticationStrategy)

    /**
     * Verify the mocks
     */
    @AfterEach
    fun verify() {
        confirmVerified(startAuthenticationStrategy)
    }

    /**
     * Test that we can get the Provider ID back out
     */
    @Test
    fun testProviderId() {
        expect(testSubject.providerId).toBe("testing")
    }

    /**
     * Test starting authentication works as expected
     */
    @Test
    fun testStartAuthentication() {
        val startAuthenticationResult = StartAuthenticationResult(URI("http://google.com"))

        every { startAuthenticationStrategy.startAuthentication() } returns startAuthenticationResult

        expect(testSubject.startAuthentication()).toBe(startAuthenticationResult)

        verify(exactly = 1) { startAuthenticationStrategy.startAuthentication() }
    }

    /**
     * Test completing authentication works as expected
     */
    @Test
    fun testCompleteAuthentication() {
        val externalUser = ExternalUser(
                userId = "userId",
                displayName = "Display Name",
                name = "Name",
                email = "Email"
        )
        val params = mapOf("state" to UUID.randomUUID().toString())

        every { completeAuthenticationStrategy.completeAuthentication(params) } returns externalUser

        expect(testSubject.completeAuthentication(params)).toBe(externalUser)

        verify(exactly = 1) { completeAuthenticationStrategy.completeAuthentication(params) }
    }
}
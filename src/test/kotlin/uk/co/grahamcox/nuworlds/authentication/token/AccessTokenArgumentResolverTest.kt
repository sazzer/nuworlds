package uk.co.grahamcox.nuworlds.authentication.token

import ch.tutteli.atrium.api.cc.en_GB.toBe
import ch.tutteli.atrium.api.cc.en_GB.toThrow
import ch.tutteli.atrium.verbs.expect
import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.TestFactory
import org.springframework.core.MethodParameter
import org.springframework.mock.web.MockHttpServletRequest
import org.springframework.mock.web.MockHttpServletResponse
import org.springframework.web.context.request.ServletWebRequest
import uk.co.grahamcox.nuworlds.users.UserId
import java.util.*
import javax.servlet.http.Cookie
import kotlin.reflect.jvm.javaMethod

/**
 * Unit tests for the Access Token Argument Resolver
 */
internal class AccessTokenArgumentResolverTest {
    /** The mock Access Token Serializer */
    private val accessTokenSerializer = mockk<AccessTokenSerializer>()

    /** The test subject */
    private val testSubject = AccessTokenArgumentResolver(accessTokenSerializer)

    /**
     * A test class as a handler
     */
    class Handler {
        fun stringArg(arg: String) {}

        fun userIdArg(arg: UserId) {}

        fun accessTokenArg(arg: AccessToken) {}

        fun optionalUserIdArg(arg: UserId?) {}

        fun optionalAccessTokenArg(arg: AccessToken?) {}

        fun mixedArgs(userId: UserId, accessToken: AccessToken, other: String) {}
    }

    @TestFactory
    fun testSupportsParameter() = listOf(
            MethodParameter(Handler::stringArg.javaMethod!!, 0) to false,

            MethodParameter(Handler::userIdArg.javaMethod!!, 0) to true,
            MethodParameter(Handler::accessTokenArg.javaMethod!!, 0) to true,
            MethodParameter(Handler::optionalUserIdArg.javaMethod!!, 0) to true,
            MethodParameter(Handler::optionalAccessTokenArg.javaMethod!!, 0) to true,

            MethodParameter(Handler::mixedArgs.javaMethod!!, 0) to true,
            MethodParameter(Handler::mixedArgs.javaMethod!!, 1) to true,
            MethodParameter(Handler::mixedArgs.javaMethod!!, 2) to false
    ).map {
        DynamicTest.dynamicTest(it.toString()) {
            expect(testSubject.supportsParameter(it.first)).toBe(it.second)
        }
    }

    @TestFactory
    fun testProvideArgumentSuccess(): List<DynamicTest> {
        val userId = UserId(UUID.randomUUID())
        val accessToken = AccessToken(
                userId = userId,
                tokenId = UUID.randomUUID()
        )

        every { accessTokenSerializer.deserialize("serializedToken") } returns(accessToken)

        val realRequest = MockHttpServletRequest()
        realRequest.setCookies(Cookie("nuworlds_access_token", "serializedToken"))
        val request = ServletWebRequest(realRequest, MockHttpServletResponse())

        return listOf(
                MethodParameter(Handler::userIdArg.javaMethod!!, 0) to userId,
                MethodParameter(Handler::accessTokenArg.javaMethod!!, 0) to accessToken,

                MethodParameter(Handler::optionalUserIdArg.javaMethod!!, 0) to userId,
                MethodParameter(Handler::optionalAccessTokenArg.javaMethod!!, 0) to accessToken,

                MethodParameter(Handler::mixedArgs.javaMethod!!, 0) to userId,
                MethodParameter(Handler::mixedArgs.javaMethod!!, 1) to accessToken
        ).map {
            DynamicTest.dynamicTest(it.toString()) {
                val resolved = testSubject.resolveArgument(it.first, null, request, null)
                expect(resolved).toBe(it.second)
            }
        }
    }

    @TestFactory
    fun testMissingAccessTokenSuccess(): List<DynamicTest> {
        val realRequest = MockHttpServletRequest()
        val request = ServletWebRequest(realRequest, MockHttpServletResponse())

        return listOf(
                MethodParameter(Handler::optionalUserIdArg.javaMethod!!, 0) to null,
                MethodParameter(Handler::optionalAccessTokenArg.javaMethod!!, 0) to null
        ).map {
            DynamicTest.dynamicTest(it.toString()) {
                val resolved = testSubject.resolveArgument(it.first, null, request, null)
                expect(resolved).toBe(it.second)
            }
        }
    }

    @TestFactory
    fun testMissingAccessTokenFailure(): List<DynamicTest> {
        val realRequest = MockHttpServletRequest()
        val request = ServletWebRequest(realRequest, MockHttpServletResponse())

        return listOf(
                MethodParameter(Handler::userIdArg.javaMethod!!, 0),
                MethodParameter(Handler::accessTokenArg.javaMethod!!, 0),

                MethodParameter(Handler::mixedArgs.javaMethod!!, 0),
                MethodParameter(Handler::mixedArgs.javaMethod!!, 1)
        ).map {
            DynamicTest.dynamicTest(it.toString()) {
                expect {
                    testSubject.resolveArgument(it, null, request, null)
                }.toThrow<MissingAccessTokenException> {  }
            }
        }
    }

    @TestFactory
    fun testInvalidAccessTokenFailure(): List<DynamicTest> {
        every { accessTokenSerializer.deserialize("serializedToken") } throws(InvalidAccessTokenException("Oops"))

        val realRequest = MockHttpServletRequest()
        realRequest.setCookies(Cookie("nuworlds_access_token", "serializedToken"))
        val request = ServletWebRequest(realRequest, MockHttpServletResponse())

        return listOf(
                MethodParameter(Handler::userIdArg.javaMethod!!, 0),
                MethodParameter(Handler::accessTokenArg.javaMethod!!, 0),

                MethodParameter(Handler::mixedArgs.javaMethod!!, 0),
                MethodParameter(Handler::mixedArgs.javaMethod!!, 1)
        ).map {
            DynamicTest.dynamicTest(it.toString()) {
                expect {
                    testSubject.resolveArgument(it, null, request, null)
                }.toThrow<InvalidAccessTokenException> {  }
            }
        }
    }
}

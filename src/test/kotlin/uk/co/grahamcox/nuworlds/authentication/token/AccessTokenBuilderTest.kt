package uk.co.grahamcox.nuworlds.authentication.token

import ch.tutteli.atrium.api.cc.en_GB.notToBe
import ch.tutteli.atrium.api.cc.en_GB.property
import ch.tutteli.atrium.api.cc.en_GB.toBe
import ch.tutteli.atrium.verbs.expect
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import uk.co.grahamcox.nuworlds.model.Identity
import uk.co.grahamcox.nuworlds.users.UserData
import uk.co.grahamcox.nuworlds.users.UserId
import uk.co.grahamcox.nuworlds.users.UserModel
import java.time.Instant
import java.util.*

/**
 * Unit tests for the Access Token Builder
 */
internal class AccessTokenBuilderTest {
    /** The test subject */
    private val testSubject = AccessTokenBuilder()

    /**
     * Test building an access token
     */
    @Test
    fun test() {
        val user = UserModel(
                identity = Identity(
                        id = UserId(UUID.randomUUID()),
                        version = UUID.randomUUID(),
                        created = Instant.now(),
                        updated = Instant.now()
                ),
                data = UserData(
                        name = "Test User",
                        email = "test@example.com",
                        authenticationDetails = emptySet()
                )
        )

        val token = testSubject.buildAccessToken(user)

        expect(token) {
            property(subject::userId).toBe(user.identity.id)
            property(subject::tokenId).notToBe(user.identity.id.id)
            property(subject::tokenId).notToBe(user.identity.version)
        }
    }
}
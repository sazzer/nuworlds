package uk.co.grahamcox.nuworlds.authentication.external

import ch.tutteli.atrium.api.cc.en_GB.*
import org.junit.jupiter.api.Test
import ch.tutteli.atrium.verbs.expect
import io.mockk.every
import io.mockk.mockk

/**
 * Unit tests for the External Authentication Provider Registry
 */
internal class ExternalAuthenticationProviderRegistryTest {
    private val google = mockk<ExternalAuthenticationProvider>("google") {
        every { providerId } returns "google plus"
    }
    private val facebook = mockk<ExternalAuthenticationProvider>("facebook") {
        every { providerId } returns "facebook"
    }

    /**
     * Test getting the list of Provider IDs from an empty registry
     */
    @Test
    fun getNoProviderIds() {
        val testSubject = ExternalAuthenticationProviderRegistry(listOf())

        expect(testSubject.providerIds).isEmpty()
    }

    /**
     * Test getting a provider by ID from an empty registry
     */
    @Test
    fun getNoProvider() {
        val testSubject = ExternalAuthenticationProviderRegistry(listOf())

        expect(testSubject.get("google plus")).toBe(null)
        expect(testSubject["google plus"]).toBe(null)
    }

    /**
     * Test getting the list of Provider IDs from a populated registry
     */
    @Test
    fun getProviderIds() {
        val testSubject = ExternalAuthenticationProviderRegistry(listOf(google, facebook))

        expect(testSubject.providerIds).contains.inOrder.only.values("google plus","facebook")
    }

    /**
     * Test getting an unknown provider by ID from a populated registry
     */
    @Test
    fun getUnknownProvider() {
        val testSubject = ExternalAuthenticationProviderRegistry(listOf(google, facebook))

        expect(testSubject.get("unknown")).toBe(null)
        expect(testSubject["unknown"]).toBe(null)
    }

    /**
     * Test getting a known provider by ID from a populated registry
     */
    @Test
    fun getKnownProvider() {
        val testSubject = ExternalAuthenticationProviderRegistry(listOf(google, facebook))

        expect(testSubject.get("google plus")).toBe(google)
        expect(testSubject["facebook"]).toBe(facebook)
    }
}
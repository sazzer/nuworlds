package uk.co.grahamcox.nuworlds.authentication.token

import ch.tutteli.atrium.api.cc.en_GB.*
import ch.tutteli.atrium.verbs.expect
import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import org.junit.jupiter.api.Test
import uk.co.grahamcox.nuworlds.users.UserId
import java.util.*

/**
 * Unit tests for the Access Token Serializer
 */
internal class JwtAccessTokenSerializerImplTest {
    /** The signing algorithm */
    private val algorithm = Algorithm.HMAC512(UUID.randomUUID().toString())

    /** The test subject */
    private val testSubject = JwtAccessTokenSerializerImpl(algorithm)

    @Test
    fun testSerializeToken() {
        val token = AccessToken(
                userId = UserId(UUID.randomUUID()),
                tokenId = UUID.randomUUID()
        )

        val serialized = testSubject.serialize(token)

        expect(JWT.require(algorithm).build().verify(serialized)) {
            returnValueOf(subject::getClaims).hasSize(2)
            returnValueOf(subject::getSubject).toBe(token.userId.id.toString())
            returnValueOf(subject::getId).toBe(token.tokenId.toString())
        }
    }

    @Test
    fun testDeserializeToken() {
        val userId = UUID.randomUUID()
        val tokenId = UUID.randomUUID()

        val serialized = JWT.create()
                .withSubject(userId.toString())
                .withJWTId(tokenId.toString())
                .sign(algorithm)

        expect(testSubject.deserialize(serialized)) {
            property(subject::userId).toBe(UserId(userId))
            property(subject::tokenId).toBe(tokenId)
        }
    }

    @Test
    fun testDeserializeInvalidToken() {
        expect { testSubject.deserialize("invalid") }.toThrow<InvalidAccessTokenException> {  }
    }

    @Test
    fun testDeserializeInvalidUserId() {
        val serialized = JWT.create()
                .withSubject("invalid")
                .withJWTId(UUID.randomUUID().toString())
                .sign(algorithm)

        expect { testSubject.deserialize(serialized) }.toThrow<InvalidAccessTokenException> {  }
    }

    @Test
    fun testDeserializeInvalidTokenId() {
        val serialized = JWT.create()
                .withJWTId("invalid")
                .withSubject(UUID.randomUUID().toString())
                .sign(algorithm)

        expect { testSubject.deserialize(serialized) }.toThrow<InvalidAccessTokenException> {  }
    }

    @Test
    fun testDeserializeMissingUserId() {
        val serialized = JWT.create()
                .withJWTId(UUID.randomUUID().toString())
                .sign(algorithm)

        expect { testSubject.deserialize(serialized) }.toThrow<InvalidAccessTokenException> {  }
    }

    @Test
    fun testDeserializeMissingTokenId() {
        val serialized = JWT.create()
                .withSubject(UUID.randomUUID().toString())
                .sign(algorithm)

        expect { testSubject.deserialize(serialized) }.toThrow<InvalidAccessTokenException> {  }
    }
}
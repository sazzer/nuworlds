package uk.co.grahamcox.nuworlds.authentication.external.google

import ch.tutteli.atrium.api.cc.en_GB.contains
import ch.tutteli.atrium.api.cc.en_GB.containsKey
import ch.tutteli.atrium.api.cc.en_GB.toBe
import ch.tutteli.atrium.verbs.expect
import org.junit.jupiter.api.Test
import org.springframework.web.util.UriComponentsBuilder
import java.net.URI

/**
 * Unit Test for the Google Start Authentication Strategy
 */
internal class GoogleStartAuthenticationStrategyTest {
    /** The settings to work with */
    private val settings = GoogleSettings(
            clientId = "googleClientId",
            clientSecret = "googleClientSecret",
            callbackUrl = URI("http://localhost:8080/login/google_plus/callback"),
            redirectUrl = URI("https://accounts.google.com/o/oauth2/v2/auth"),
            tokenUrl = URI("https://www.googleapis.com/oauth2/v4/token")
    )

    /** The test subject */
    private val testSubject = GoogleStartAuthenticationStrategy(settings)

    /**
     * Test starting authentication
     */
    @Test
    fun testStartAuthentication() {
        val authentication = testSubject.startAuthentication()

        val uriComponents = UriComponentsBuilder.fromUri(authentication.redirectUrl).build()

        expect(uriComponents.scheme).toBe("https")
        expect(uriComponents.host).toBe("accounts.google.com")
        expect(uriComponents.path).toBe("/o/oauth2/v2/auth")

        expect(uriComponents.queryParams).contains("client_id" to listOf("googleClientId"))
        expect(uriComponents.queryParams).contains("redirect_uri" to listOf("http://localhost:8080/login/google_plus/callback"))
        expect(uriComponents.queryParams).contains("response_type" to listOf("code"))
        expect(uriComponents.queryParams).contains("scope" to listOf("openid%20email%20profile"))
        expect(uriComponents.queryParams).containsKey("state")
    }
}
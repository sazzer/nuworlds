package uk.co.grahamcox.nuworlds.authentication.external.google

import ch.tutteli.atrium.api.cc.en_GB.property
import ch.tutteli.atrium.api.cc.en_GB.toBe
import ch.tutteli.atrium.api.cc.en_GB.toThrow
import ch.tutteli.atrium.verbs.expect
import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import org.junit.jupiter.api.Test
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType
import org.springframework.test.web.client.MockRestServiceServer
import org.springframework.test.web.client.match.MockRestRequestMatchers.*
import org.springframework.test.web.client.response.MockRestResponseCreators.*
import org.springframework.util.LinkedMultiValueMap
import uk.co.grahamcox.nuworlds.authentication.external.ExternalProviderFailureException
import java.net.URI

/**
 * Tests for the Google Complete Authentication Strategy
 */
internal class GoogleCompleteAuthenticationStrategyTest {
    /** The settings to work with */
    private val settings = GoogleSettings(
            clientId = "googleClientId",
            clientSecret = "googleClientSecret",
            callbackUrl = URI("http://localhost:8080/login/google_plus/callback"),
            redirectUrl = URI("https://accounts.google.com/o/oauth2/v2/auth"),
            tokenUrl = URI("https://www.googleapis.com/oauth2/v4/token")
    )

    /** The Rest Template to work with */
    private val restTemplate = RestTemplateBuilder().build()

    /** The mock server that the Rest Template talks to */
    private val server = MockRestServiceServer.createServer(restTemplate)

    /** The test subject */
    private val testSubject = GoogleCompleteAuthenticationStrategy(settings, restTemplate)

    /**
     * Test when there was no authorization code returned to us
     */
    @Test
    fun testNoCode() {
        expect {
            testSubject.completeAuthentication(mapOf())
        }.toThrow<ExternalProviderFailureException>{}
    }

    /**
     * Test when Google is responding with a 500 Internal Server Error
     */
    @Test
    fun testGoogleDown() {
        server.expect(requestTo("https://www.googleapis.com/oauth2/v4/token"))
                .andExpect(method(HttpMethod.POST))
                .andExpect(content().contentTypeCompatibleWith("application/x-www-form-urlencoded"))
                .andExpect(content().formData(LinkedMultiValueMap(
                        mapOf(
                                "code" to listOf("authCode"),
                                "client_id" to listOf("googleClientId"),
                                "client_secret" to listOf("googleClientSecret"),
                                "redirect_uri" to listOf("http://localhost:8080/login/google_plus/callback"),
                                "grant_type" to listOf("authorization_code")
                        )
                )))
                .andRespond(withServerError())

        expect {
            testSubject.completeAuthentication(mapOf("code" to "authCode"))
        }.toThrow<ExternalProviderFailureException>{}
    }

    /**
     * Test when the call to Google responds that the authorization code was invalid
     */
    @Test
    fun testInvalidCode() {
        server.expect(requestTo("https://www.googleapis.com/oauth2/v4/token"))
                .andExpect(method(HttpMethod.POST))
                .andExpect(content().contentTypeCompatibleWith("application/x-www-form-urlencoded"))
                .andExpect(content().formData(LinkedMultiValueMap(
                        mapOf(
                                "code" to listOf("authCode"),
                                "client_id" to listOf("googleClientId"),
                                "client_secret" to listOf("googleClientSecret"),
                                "redirect_uri" to listOf("http://localhost:8080/login/google_plus/callback"),
                                "grant_type" to listOf("authorization_code")
                        )
                )))
                .andRespond(withBadRequest()
                        .body("""{
                              "error": "invalid_grant",
                              "error_description": "Bad Request"
                            }""")
                        .contentType(MediaType.APPLICATION_JSON))

        expect {
            testSubject.completeAuthentication(mapOf("code" to "authCode"))
        }.toThrow<ExternalProviderFailureException>{}
    }

    /**
     * Test when Google responds with a success but no response body.
     * This can't actually happen in real life.
     */
    @Test
    fun testNoResponsePayload() {
        server.expect(requestTo("https://www.googleapis.com/oauth2/v4/token"))
                .andExpect(method(HttpMethod.POST))
                .andExpect(content().contentTypeCompatibleWith("application/x-www-form-urlencoded"))
                .andExpect(content().formData(LinkedMultiValueMap(
                        mapOf(
                                "code" to listOf("authCode"),
                                "client_id" to listOf("googleClientId"),
                                "client_secret" to listOf("googleClientSecret"),
                                "redirect_uri" to listOf("http://localhost:8080/login/google_plus/callback"),
                                "grant_type" to listOf("authorization_code")
                        )
                )))
                .andRespond(withSuccess())

        expect {
            testSubject.completeAuthentication(mapOf("code" to "authCode"))
        }.toThrow<ExternalProviderFailureException>{}
    }

    /**
     * Test when Google responds with a success but no ID Token
     */
    @Test
    fun testNoIdToken() {
        server.expect(requestTo("https://www.googleapis.com/oauth2/v4/token"))
                .andExpect(method(HttpMethod.POST))
                .andExpect(content().contentTypeCompatibleWith("application/x-www-form-urlencoded"))
                .andExpect(content().formData(LinkedMultiValueMap(
                        mapOf(
                                "code" to listOf("authCode"),
                                "client_id" to listOf("googleClientId"),
                                "client_secret" to listOf("googleClientSecret"),
                                "redirect_uri" to listOf("http://localhost:8080/login/google_plus/callback"),
                                "grant_type" to listOf("authorization_code")
                        )
                )))
                .andRespond(withSuccess()
                        .body("""{
                                "access_token": "accessTokenHere",
                                "expires_in": 3589,
                                "scope": "https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email openid",
                                "token_type": "Bearer"
                            }""")
                        .contentType(MediaType.APPLICATION_JSON))

        expect {
            testSubject.completeAuthentication(mapOf("code" to "authCode"))
        }.toThrow<ExternalProviderFailureException>{}
    }

    /**
     * Test when Google successfully authenticates the user
     */
    @Test
    fun testSuccess() {
        val idToken = JWT.create()
                .withSubject("12345678")
                .withClaim("email", "test@example.com")
                .withClaim("name", "Test User")
                .sign(Algorithm.HMAC256("Testing"))

        server.expect(requestTo("https://www.googleapis.com/oauth2/v4/token"))
                .andExpect(method(HttpMethod.POST))
                .andExpect(content().contentTypeCompatibleWith("application/x-www-form-urlencoded"))
                .andExpect(content().formData(LinkedMultiValueMap(
                        mapOf(
                                "code" to listOf("authCode"),
                                "client_id" to listOf("googleClientId"),
                                "client_secret" to listOf("googleClientSecret"),
                                "redirect_uri" to listOf("http://localhost:8080/login/google_plus/callback"),
                                "grant_type" to listOf("authorization_code")
                        )
                )))
                .andRespond(withSuccess()
                        .body("""{
                                "access_token": "accessTokenHere",
                                "expires_in": 3589,
                                "scope": "https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email openid",
                                "token_type": "Bearer",
                                "id_token": "$idToken"
                            }""")
                        .contentType(MediaType.APPLICATION_JSON))

        val externalUser = testSubject.completeAuthentication(mapOf("code" to "authCode"))

        expect(externalUser) {
            property(subject::userId).toBe("12345678")
            property(subject::displayName).toBe("test@example.com")
            property(subject::name).toBe("Test User")
            property(subject::email).toBe("test@example.com")
        }
    }

    /**
     * Test when Google is responding with a malformed ID Token
     */
    @Test
    fun testMalformedIDToken() {
        server.expect(requestTo("https://www.googleapis.com/oauth2/v4/token"))
                .andExpect(method(HttpMethod.POST))
                .andExpect(content().contentTypeCompatibleWith("application/x-www-form-urlencoded"))
                .andExpect(content().formData(LinkedMultiValueMap(
                        mapOf(
                                "code" to listOf("authCode"),
                                "client_id" to listOf("googleClientId"),
                                "client_secret" to listOf("googleClientSecret"),
                                "redirect_uri" to listOf("http://localhost:8080/login/google_plus/callback"),
                                "grant_type" to listOf("authorization_code")
                        )
                )))
                .andRespond(withSuccess()
                        .body("""{
                                "access_token": "accessTokenHere",
                                "expires_in": 3589,
                                "scope": "https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email openid",
                                "token_type": "Bearer",
                                "id_token": "i.am.malformed"
                            }""")
                        .contentType(MediaType.APPLICATION_JSON))

        expect {
            testSubject.completeAuthentication(mapOf("code" to "authCode"))
        }.toThrow<ExternalProviderFailureException>{}
    }

    /**
     * Test when Google is responding with an ID Token with no subject field
     */
    @Test
    fun testMissingSubject() {
        val idToken = JWT.create()
                .withClaim("email", "test@example.com")
                .withClaim("name", "Test User")
                .sign(Algorithm.HMAC256("Testing"))

        server.expect(requestTo("https://www.googleapis.com/oauth2/v4/token"))
                .andExpect(method(HttpMethod.POST))
                .andExpect(content().contentTypeCompatibleWith("application/x-www-form-urlencoded"))
                .andExpect(content().formData(LinkedMultiValueMap(
                        mapOf(
                                "code" to listOf("authCode"),
                                "client_id" to listOf("googleClientId"),
                                "client_secret" to listOf("googleClientSecret"),
                                "redirect_uri" to listOf("http://localhost:8080/login/google_plus/callback"),
                                "grant_type" to listOf("authorization_code")
                        )
                )))
                .andRespond(withSuccess()
                        .body("""{
                                "access_token": "accessTokenHere",
                                "expires_in": 3589,
                                "scope": "https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email openid",
                                "token_type": "Bearer",
                                "id_token": "$idToken"
                            }""")
                        .contentType(MediaType.APPLICATION_JSON))

        expect {
            testSubject.completeAuthentication(mapOf("code" to "authCode"))
        }.toThrow<ExternalProviderFailureException>{}
    }

    /**
     * Test when Google is responding with an ID Token with no email field
     */
    @Test
    fun testMissingEmail() {
        val idToken = JWT.create()
                .withSubject("12345678")
                .withClaim("name", "Test User")
                .sign(Algorithm.HMAC256("Testing"))

        server.expect(requestTo("https://www.googleapis.com/oauth2/v4/token"))
                .andExpect(method(HttpMethod.POST))
                .andExpect(content().contentTypeCompatibleWith("application/x-www-form-urlencoded"))
                .andExpect(content().formData(LinkedMultiValueMap(
                        mapOf(
                                "code" to listOf("authCode"),
                                "client_id" to listOf("googleClientId"),
                                "client_secret" to listOf("googleClientSecret"),
                                "redirect_uri" to listOf("http://localhost:8080/login/google_plus/callback"),
                                "grant_type" to listOf("authorization_code")
                        )
                )))
                .andRespond(withSuccess()
                        .body("""{
                                "access_token": "accessTokenHere",
                                "expires_in": 3589,
                                "scope": "https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email openid",
                                "token_type": "Bearer",
                                "id_token": "$idToken"
                            }""")
                        .contentType(MediaType.APPLICATION_JSON))

        expect {
            testSubject.completeAuthentication(mapOf("code" to "authCode"))
        }.toThrow<ExternalProviderFailureException>{}
    }

    /**
     * Test when Google is responding with an ID Token with no name field
     */
    @Test
    fun testMissingName() {
        val idToken = JWT.create()
                .withSubject("12345678")
                .withClaim("email", "test@example.com")
                .sign(Algorithm.HMAC256("Testing"))

        server.expect(requestTo("https://www.googleapis.com/oauth2/v4/token"))
                .andExpect(method(HttpMethod.POST))
                .andExpect(content().contentTypeCompatibleWith("application/x-www-form-urlencoded"))
                .andExpect(content().formData(LinkedMultiValueMap(
                        mapOf(
                                "code" to listOf("authCode"),
                                "client_id" to listOf("googleClientId"),
                                "client_secret" to listOf("googleClientSecret"),
                                "redirect_uri" to listOf("http://localhost:8080/login/google_plus/callback"),
                                "grant_type" to listOf("authorization_code")
                        )
                )))
                .andRespond(withSuccess()
                        .body("""{
                                "access_token": "accessTokenHere",
                                "expires_in": 3589,
                                "scope": "https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email openid",
                                "token_type": "Bearer",
                                "id_token": "$idToken"
                            }""")
                        .contentType(MediaType.APPLICATION_JSON))

        expect {
            testSubject.completeAuthentication(mapOf("code" to "authCode"))
        }.toThrow<ExternalProviderFailureException>{}
    }
}
FROM adoptopenjdk/openjdk13:jdk-13_33-alpine-slim

RUN apk add curl gettext

ENV PORT=8080

ENV NUWORLDS_LOGGING_LEVEL=INFO

ENV NUWORLDS_PG_URL=

ENV NUWORLDS_GOOGLE_REDIRECTURL=https://accounts.google.com/o/oauth2/v2/auth
ENV NUWORLDS_GOOGLE_TOKENURL=https://www.googleapis.com/oauth2/v4/token
ENV NUWORLDS_GOOGLE_CALLBACKURL=
ENV NUWORLDS_GOOGLE_CLIENTID=
ENV NUWORLDS_GOOGLE_CLIENTSECRET=

ARG JAR_FILE
RUN echo JAR_FILE=${JAR_FILE}

ADD ${JAR_FILE} /opt/nuworlds/service.jar
ADD src/main/docker/application.properties.template /opt/nuworlds/application.properties.template
ADD src/main/docker/start.sh /opt/nuworlds/start.sh

WORKDIR /opt/nuworlds
CMD /opt/nuworlds/start.sh

HEALTHCHECK CMD curl -f http://localhost:8080/actuator/health || exit 1
